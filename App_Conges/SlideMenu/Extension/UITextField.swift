//
//  UITextField.swift
//  App_Conges
//
//  Created by Hamza on 7/29/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
import UIKit
extension UITextField {
    func modifyClearButtonWithImage(image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 40, height:40)

        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear), for: .touchUpInside)
        self.rightView = clearButton
        self.rightViewMode = .whileEditing
    }
    
    func clear(sender : AnyObject) {
        self.text = ""
    }
    
}
