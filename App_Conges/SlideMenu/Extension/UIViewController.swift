//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named:"icn-menu")!)
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    func changeColorOfOneCaractarFromLabel(label:UILabel,caractarPosition:Int){
        
        let   mutableString = NSMutableAttributedString(string: label.text!, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 18.0)!])
        mutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location:label.text!.length-1,length:1))
        // set label Attribute
        label.attributedText = mutableString
    }
    func compareDate(date1:Date,date2:Date) -> Bool{
        
        let year1 = Calendar.current.component(.year, from: date1)
        let month1 = Calendar.current.component(.month, from: date1)
        let day1 = Calendar.current.component(.day, from: date1)
        let year2 = Calendar.current.component(.year, from: date2)
        let month2 = Calendar.current.component(.month, from: date2)
        let day2 = Calendar.current.component(.day, from: date2)
        if year1 == year2 {
            if month1 == month2 {
                if day1 == day2 {
                    return false
                }else{return day1>day2}
            }else{return month1>month2}
        }else{return year1>year2}
        
        
    }
    
    
    
}
