
//
//  SplashViewController.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/19/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class SplashViewController: ViewController {
    
    var timer : Timer!
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        timer =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(loadLoginVC), userInfo: nil, repeats: false)
        
        
    }
    
    @objc func loadLoginVC()   {
        
        let loginVC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        self.present(loginVC!, animated: true, completion: nil)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

