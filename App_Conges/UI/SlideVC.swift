//
//  SlideVC.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/18/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import SCLAlertView

enum LeftMenu: Int {
case dashBoard = 0
case nouvelleDemande
case joursFerier
case logout
}

class SlideVC:ViewController{
    
    //MARK: Static
    static  var  s_isDashBoardVC = true
    static  var  s_isNouvelleDemandeVC = false
    static  var  s_isJoursFeriesViewController = false
    
    //MARK: Outlet
    @IBOutlet var tabView: UITableView!
    @IBOutlet weak var profileImg: RoundImage!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var societeLbl: UILabel!
    //MARK: Variable
    var dashBoard:UINavigationController!
    var nouvelleDemande:UINavigationController!
    var joursFerierVC:UINavigationController!
    var logout:UINavigationController!

    let imgTab = [UIImage(named:"dashboard_icn"),UIImage(named:"plus"),UIImage(named:"jrsFeries_icn"),UIImage(named:"logout")]
    let lblTxtTab = ["DASHBOARD","NOUVELLE DEMANDE","JOURS FERIÉS","LOGOUT"]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        API.SlideVCReference = self
        tabView.dataSource = self
        tabView.delegate = self

    
        
        let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "JoursFeriesViewController") as! JoursFeriesViewController
        self.joursFerierVC = UINavigationController(rootViewController: joursFerier)
        joursFerierVC.navigationBar.setBackgroundImage(UIImage(), for: .default)
        joursFerierVC.navigationBar.shadowImage = UIImage()
        joursFerierVC.navigationBar.isTranslucent = true
        joursFerierVC.navigationBar.tintColor = UIColor.white
        joursFerierVC.navigationBar.barStyle = UIBarStyle.black
    }
    override func viewDidLayoutSubviews() {
        profileImg.awakeFromNib()
    }
    
    func changeViewController(menu : LeftMenu){
        switch menu {
        case .dashBoard:
            
            let dash =  storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.dashBoard = UINavigationController(rootViewController: dash)
            
            dashBoard.navigationBar.setBackgroundImage(UIImage(), for: .default)
            dashBoard.navigationBar.shadowImage = UIImage()
            dashBoard.navigationBar.isTranslucent = true
            dashBoard.navigationBar.tintColor = UIColor.white
            dashBoard.navigationBar.barStyle = UIBarStyle.black
             self.slideMenuController()?.changeMainViewController(dashBoard, close: true)
      
        case .nouvelleDemande:
            let nv =  storyboard?.instantiateViewController(withIdentifier: "NouvelleDemandeVC") as! NouvelleDemandeVC
            self.nouvelleDemande = UINavigationController(rootViewController: nv)
            nouvelleDemande.navigationBar.setBackgroundImage(UIImage(), for: .default)
            nouvelleDemande.navigationBar.shadowImage = UIImage()
            nouvelleDemande.navigationBar.isTranslucent = true
            nouvelleDemande.navigationBar.tintColor = UIColor.white
            nouvelleDemande.navigationBar.barStyle = UIBarStyle.black
             self.slideMenuController()?.changeMainViewController(nouvelleDemande, close: true)
        case .joursFerier:
            let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "JoursFeriesViewController") as! JoursFeriesViewController
            self.joursFerierVC = UINavigationController(rootViewController: joursFerier)
            joursFerierVC.navigationBar.setBackgroundImage(UIImage(), for: .default)
            joursFerierVC.navigationBar.shadowImage = UIImage()
            joursFerierVC.navigationBar.isTranslucent = true
            joursFerierVC.navigationBar.tintColor = UIColor.white
            joursFerierVC.navigationBar.barStyle = UIBarStyle.black
             self.slideMenuController()?.changeMainViewController(joursFerierVC, close: true)
        case.logout:
            API.logout(onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
            if succes {
                if json?["success"] as! Bool {
                    self.dismiss(animated: true, completion: nil)
                    //self.slideMenuController()?.changeMainViewController(self.logout, close: true)
                    UIApplication.shared.delegate?.window??.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                    
                }else{
                    SCLAlertView().showTitle(
                        "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    
                    
                }
            }else{
                SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                
            }
            
        })
            
       
            
        }
        
    }
    
}

//MARK: Extension

    //MARK: UITableView


    extension SlideVC:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}

    extension SlideVC:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Compte"
        }else{
            return "Paramétres"
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        else {return 1}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabView.dequeueReusableCell(withIdentifier:"MenuCell") as! MenuCell
        if indexPath.section == 0 {
            
            cell.lbl.text = lblTxtTab[indexPath.row]
            cell.img.image = imgTab[indexPath.row]
            
        }else {
            cell.lbl.text = lblTxtTab[3]
            cell.img.image = imgTab[3]
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let menu = LeftMenu(rawValue: indexPath.row){
           self.changeViewController(menu: menu)
            }
 

        }
    }
    
}


