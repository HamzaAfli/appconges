//
//  JoursFeriesViewController.swift
//  App_Conges
//
//  Created by Hamza on 7/26/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class JoursFeriesViewController: ViewController {
    
    //MARK:- Variable
    var joursFeriesTab:[Holyday] = []

    @IBOutlet weak var tabView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        API.joursFeriesViewControllerReference = self
        API.getHolydays()
        tabView.delegate = self
        tabView.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "JOURS FERIÉS"
        
    }
    

}


//MARK:-  Extension
    //MARK: SlideMenu

extension JoursFeriesViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
    
    
    
    
}
        //MARK: TableView

extension JoursFeriesViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension JoursFeriesViewController:UITableViewDataSource{
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return joursFeriesTab.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabView.dequeueReusableCell(withIdentifier:"JoursFerierCell") as! JoursFerierCell
        
            cell.titleLbl.text = joursFeriesTab[indexPath.row].name
            cell.dateLbl.text =  joursFeriesTab[indexPath.row].date
        if(indexPath.row % 2 == 0){
            cell.rectImg.image = UIImage(named:"rect")
        }else{
            cell.rectImg.image = UIImage()
        }
        
        return cell
    
    
    
}
}
