//
//  NouvelleDemandeCongesVC.swift
//  App_Conges
//
//  Created by Hamza on 8/16/17.
//  Copyright © 2017 Hamza. All rights reserved.
//


    
    import UIKit
    import DGRunkeeperSwitch
    import IQKeyboardManagerSwift
    import  SCLAlertView
    
    class NouvelleDemandeCongesVC: ViewController {
        
        //MARK:-  Outlet
        
        
        @IBOutlet weak var dateDebTF: UITextField!
        
        
        @IBOutlet weak var rqView: UIView!
        
        
        @IBOutlet weak var dateFinTF: UITextField!
        
        
        @IBOutlet weak var rqView2: UIView!
        
        
        @IBOutlet weak var categorieTF: UITextField!
        
        
        
        @IBOutlet weak var motifTF: UITextField!
        
        
        @IBOutlet weak var underDateDevView: UIView!
        
        @IBOutlet weak var annulerBtn: LoginBtn!
        
        
        @IBOutlet weak var enregistrerBtn: LoginBtn!
        
        @IBOutlet weak var dateDebLbl: UILabel!
        
        @IBOutlet weak var dateFinLbl: UILabel!
        
        @IBOutlet weak var categorieLbl: UILabel!
        
        @IBOutlet weak var underDateFinView: UIView!
        
        @IBOutlet weak var motifLBL: UILabel!
        @IBOutlet weak var underMotifView: UIView!
        @IBOutlet weak var underCategorieView: UIView!
        //MARK:-  Variable
        
        var pickCategorieOption = ["categ1","categ2","categ3","categ4"]
        let pickerViewCategorie = UIPickerView()
        let runkeeperSwitch1 = DGRunkeeperSwitch()
        let runkeeperSwitch2 = DGRunkeeperSwitch()
        var dateDeb : Date?
        var dateFin :Date?
        var toastDeb:UILabel?
        var toastFin:UILabel?
        var toastCateg:UILabel?
        override func viewDidLoad() {
            super.viewDidLoad()
            API.nouvelleDemandeCongesVCReference = self
            
            
            
            changeColorOfOneCaractarFromLabel(label: dateDebLbl, caractarPosition: (dateDebLbl.text?.length)!-1)
            changeColorOfOneCaractarFromLabel(label: dateFinLbl, caractarPosition: (dateFinLbl.text?.length)!-1)
            changeColorOfOneCaractarFromLabel(label: categorieLbl, caractarPosition: (categorieLbl.text?.length)!-1)
            
            
            
            
            //Switch
            
            IQKeyboardManager.sharedManager().previousNextDisplayMode = .alwaysShow
            
            runkeeperSwitch1.titles = ["Matin", "L'après-midi"]
            runkeeperSwitch1.backgroundColor = UIColor(red: 251/255.0, green: 141/255.0, blue: 12/255.0, alpha: 1.0)
            runkeeperSwitch1.selectedBackgroundColor = .white
            runkeeperSwitch1.titleColor = .white
            runkeeperSwitch1.selectedTitleColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
            runkeeperSwitch1.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 9)
            runkeeperSwitch1.frame = CGRect(x: 0, y: rqView.bounds.height/4, width: rqView.bounds.width-10, height: 35.0)
            runkeeperSwitch1.autoresizingMask = [.flexibleWidth]
            
            rqView.addSubview(runkeeperSwitch1)
            
            
            
            
            
            runkeeperSwitch2.titles = ["Matin", "L'après-midi"]
            runkeeperSwitch2.backgroundColor = UIColor(red: 251/255.0, green: 141/255.0, blue: 12/255.0, alpha: 1.0)
            runkeeperSwitch2.selectedBackgroundColor = .white
            runkeeperSwitch2.titleColor = .white
            runkeeperSwitch2.selectedTitleColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
            runkeeperSwitch2.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 9)
            runkeeperSwitch2.frame = CGRect(x: 0, y: rqView2.bounds.height/4, width: rqView2.bounds.width-10, height: 35.0)
            
            runkeeperSwitch2.autoresizingMask = [.flexibleWidth]
            runkeeperSwitch2.setSelectedIndex(1, animated: false)
            rqView2.addSubview(runkeeperSwitch2)
            
            
            dateDebTF.rightViewMode = .always
            dateDebTF.rightView = UIImageView(image: UIImage(named: "calendar"))
            dateFinTF.rightViewMode = .always
            dateFinTF.rightView = UIImageView(image: UIImage(named: "calendar"))
            categorieTF.rightViewMode = .always
            categorieTF.rightView = UIImageView(image: UIImage(named: "arrow-point-to-down-Black"))
            
            // Do any additional setup after loading the view.
            API.loadCategorie(onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                if succes {
                    if json?["success"] as! Bool {
                         let data = JsonCategorie(dictionary: json! as NSDictionary)
                        API.categories = (data?.categorie)!
                       self.pickCategorieOption.removeAll()
                        for cat in (data?.categorie)!{
                            self.pickCategorieOption.append(cat.name!)
                           self.pickerViewCategorie.reloadAllComponents()
                            
                                                    }
                    
                    }else{
                        SCLAlertView().showTitle(
                            "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                        
                        
                    }
                }else{
                    SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                }
                
            })

            dateDebLbl.popOut()
            dateDebTF.popOut()
            underDateDevView.popOut()
            runkeeperSwitch1.popOut()
            dateFinLbl.popOut()
            dateFinTF.popOut()
            underDateFinView.popOut()
            runkeeperSwitch2.popOut()
            categorieLbl.popOut()
            categorieTF.popOut()
            underCategorieView.popOut()
            motifLBL.popOut()
            motifTF.popOut()
            underMotifView.popOut()
            annulerBtn.popOut()
            enregistrerBtn.popOut()
            
            
            pickerViewCategorie.delegate = self
            categorieTF.inputView = pickerViewCategorie
            
        }
        
        
        override func viewDidAppear(_ animated: Bool) {
            dateDebLbl.popIn(delay:0.1)
            dateDebTF.popIn(delay:0.2)
            underDateDevView.popIn(delay:0.3)
            runkeeperSwitch1.popIn(delay:0.4)
            dateFinLbl.popIn(delay:0.5)
            dateFinTF.popIn(delay:0.6)
            underDateFinView.popIn(delay:0.7)
            runkeeperSwitch2.popIn(delay:0.8)
            categorieLbl.popIn(delay:0.9)
            categorieTF.popIn(delay:1)
            underCategorieView.popIn(delay:1.1)
            motifLBL.popIn(delay:1.2)
            motifTF.popIn(delay:1.3)
            underMotifView.popIn(delay:1.4)
            annulerBtn.popIn(delay:1.5)
            enregistrerBtn.popIn(delay:1.6)
            annulerBtn.awakeFromNib()
            enregistrerBtn.awakeFromNib()
        }
        override func viewDidLayoutSubviews() {
            enregistrerBtn.awakeFromNib()
            annulerBtn.awakeFromNib()
            
        }
        override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
            super.viewWillTransition(to: size, with: coordinator)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.setNavigationBarItem()
            
        }
        
        //MARK:-  Action
        
        @IBAction func dateDebTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.date
            
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = 100
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            
            
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChangedForDateDebTF), for: UIControlEvents.valueChanged)
        }
        
        @IBAction func dateFinTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.date
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = 100
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChangedForDateFinTF), for: UIControlEvents.valueChanged)
        }
        
        
        @IBAction func dateFinEditingdidEnd(_ sender: UITextField) {
            if dateDebTF.text != "" && dateFinTF.text != ""{
                if dateDebTF.text == dateFinTF.text {
                    runkeeperSwitch1.setSelectedIndex(0, animated: true)
                    runkeeperSwitch2.setSelectedIndex(1, animated: true)
                    
                    
                }
                if compareDate(date1: dateDeb!, date2: dateFin!) {
                    SCLAlertView().showTitle(
                        "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    dateDebTF.text = ""
                    dateFinTF.text = ""
                }
            }
        }
        
        
        @IBAction func datedebEditingDidEnd(_ sender: UITextField) {
            if dateFinTF.text != "" && dateDebTF.text != "" {
                if dateDebTF.text == dateFinTF.text {
                    runkeeperSwitch1.setSelectedIndex(0, animated: true)
                    runkeeperSwitch2.setSelectedIndex(1, animated: true)
                    
                    
                }
                if compareDate(date1: dateDeb!, date2: dateFin!) {
                    SCLAlertView().showTitle(
                        "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    dateDebTF.text = ""
                    dateFinTF.text = ""
                }
            }
            
        }
        @IBAction func enregistrerBtn(_ sender: Any) {
            if toastDeb != nil {
                toastDeb?.removeFromSuperview()
            }
            if toastFin != nil {
                toastFin?.removeFromSuperview()
            }
            if toastCateg != nil {
                toastCateg?.removeFromSuperview()
                
            }
            if categorieTF.text == "" {
                toastCateg = Toast.showNegativeMessage(message: "catégorie ç'est une champ obligatoire", textField: categorieTF, iconName: "arrow-point-to-down-Black")
               
            }
            if dateDebTF.text == "" {
                toastDeb =  Toast.showNegativeMessage(message: "Date de debut ç'est une champ obligatoire", textField: dateDebTF, iconName: "calendar")
                
            }
            if dateFinTF.text == ""{
                toastFin = Toast.showNegativeMessage(message: "Date de fin ç'est une champ obligatoire", textField: dateFinTF, iconName: "calendar")
            }
            if categorieTF.text != "" && dateDebTF.text != "" && dateFinTF.text != ""
            {
                
                API.sendNouvelleDemande(onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                    if succes {
                        if json?["success"] as! Bool {
                            SCLAlertView().showSuccess("", subTitle:  "La demande a été enregistrée avec succès.")
                            self.dateFinTF.text = ""
                            self.dateDebTF.text = ""
                            self.categorieTF.text = ""
                            self.runkeeperSwitch1.setSelectedIndex(0, animated: true)
                            self.runkeeperSwitch2.setSelectedIndex(1, animated: true)
                            self.motifTF.text = ""
                        }else{
                            SCLAlertView().showTitle(
                                "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                            
                            
                        }
                    }else{
                        SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                        
                    }
                    
                })
            }
            
        }
        
        
        @IBAction func annulerBtnPressed(_ sender: Any) {
            dateFinTF.text = ""
            dateDebTF.text = ""
            categorieTF.text = ""
            runkeeperSwitch1.setSelectedIndex(0, animated: true)
            runkeeperSwitch2.setSelectedIndex(1, animated: true)
            motifTF.text = ""
           if  API.adminSlideVCReference != nil {
                API.adminSlideVCReference?.changeViewController(menu: .dashBoard)
           }else {
            API.SlideVCReference?.changeViewController(menu: .dashBoard)
            }
            
        }
        
        //MARK:-  Private Function
        
        @objc private func datePickerValueChangedForDateDebTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            
            dateDeb = sender.date
            
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateDebTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        @objc private func datePickerValueChangedForDateFinTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFin = sender.date
            
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFinTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        
        
        
    }
    //MARK:-  Extension
    
    //MARK: SlideMenu
    
    extension NouvelleDemandeCongesVC : SlideMenuControllerDelegate {
        
        func leftWillOpen() {
            print("SlideMenuControllerDelegate: leftWillOpen")
        }
        
        func leftDidOpen() {
            print("SlideMenuControllerDelegate: leftDidOpen")
        }
        
        func leftWillClose() {
            print("SlideMenuControllerDelegate: leftWillClose")
        }
        
        func leftDidClose() {
            print("SlideMenuControllerDelegate: leftDidClose")
        }
        
        func rightWillOpen() {
            print("SlideMenuControllerDelegate: rightWillOpen")
        }
        
        func rightDidOpen() {
            print("SlideMenuControllerDelegate: rightDidOpen")
        }
        
        func rightWillClose() {
            print("SlideMenuControllerDelegate: rightWillClose")
        }
        
        func rightDidClose() {
            print("SlideMenuControllerDelegate: rightDidClose")
        }
        
        
        
        
    }
    
    
    //MARK:PickerView
    extension NouvelleDemandeCongesVC : UIPickerViewDelegate {
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        
    }
    extension NouvelleDemandeCongesVC : UIPickerViewDataSource {
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           
                return pickCategorieOption.count
            
        }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          
            return pickCategorieOption[row]
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           
                categorieTF.text = pickCategorieOption[row]
            
        }
}


