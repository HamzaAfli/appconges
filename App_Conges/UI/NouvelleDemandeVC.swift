//
//  NouvelleDemandeVC.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/21/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import DGRunkeeperSwitch
import IQKeyboardManagerSwift
import  SCLAlertView

class NouvelleDemandeVC: ViewController {
    
    //MARK:-  Outlet
   
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var DemandeTypeTF: UITextField!
    @IBOutlet weak var profileImg: RoundImage!
    
    @IBOutlet weak var heurDeTravialLbl: UILabel!
    
    @IBOutlet weak var underHeurDeTravialLbl: UILabel!
    @IBOutlet weak var noteMoyLbl: UILabel!
   
    
    @IBOutlet weak var underDemandeTypeView: UIView!
    @IBOutlet weak var underNoteMoyLbl: UILabel!
   

    @IBOutlet weak var demandeTypeLBL: UILabel!
     //MARK:-  Variable
    
    var pickOption = ["Conges","Deplacement","Autorisation d'absence"]
    let pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         API.nouvelleDemandeVCReference = self
         titleLBL.popOut()
        profileImg.popOut()
        heurDeTravialLbl.popOut()
        underHeurDeTravialLbl.popOut()
        noteMoyLbl.popOut()
        underNoteMoyLbl.popOut()
        demandeTypeLBL.popOut()
        DemandeTypeTF.popOut()
        underDemandeTypeView.popOut()
        
        
        
        
        
        
       
       
        
        
        
        DemandeTypeTF.rightViewMode = .always
        DemandeTypeTF.rightView = UIImageView(image: UIImage(named: "arrow-point-to-down"))
        DemandeTypeTF.text = pickOption[0]
        pickerView.delegate = self
        DemandeTypeTF.inputView = pickerView
        
        
           }
   
    
    override func viewDidAppear(_ animated: Bool) {
        titleLBL.popIn(delay:0.1)
        profileImg.popIn(delay:0.2)
        heurDeTravialLbl.popIn(delay:0.3)
        underHeurDeTravialLbl.popIn(delay:0.4)
        noteMoyLbl.popIn(delay:0.5)
        underNoteMoyLbl.popIn(delay:0.6)
        demandeTypeLBL.popIn(delay:0.7)
        DemandeTypeTF.popIn(delay:0.8)
        underDemandeTypeView.popIn(delay:0.9)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
         
    }
    
     //MARK:-  Action
    
    
}
//MARK:-  Extension

        //MARK: SlideMenu

    extension NouvelleDemandeVC : SlideMenuControllerDelegate {
        
        func leftWillOpen() {
            print("SlideMenuControllerDelegate: leftWillOpen")
        }
        
        func leftDidOpen() {
            print("SlideMenuControllerDelegate: leftDidOpen")
        }
        
        func leftWillClose() {
            print("SlideMenuControllerDelegate: leftWillClose")
        }
        
        func leftDidClose() {
            print("SlideMenuControllerDelegate: leftDidClose")
        }
        
        func rightWillOpen() {
            print("SlideMenuControllerDelegate: rightWillOpen")
        }
        
        func rightDidOpen() {
            print("SlideMenuControllerDelegate: rightDidOpen")
        }
        
        func rightWillClose() {
            print("SlideMenuControllerDelegate: rightWillClose")
        }
        
        func rightDidClose() {
            print("SlideMenuControllerDelegate: rightDidClose")
        }
        
        
        
        
    }


            //MARK:PickerView
    extension NouvelleDemandeVC : UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    

    }
    extension NouvelleDemandeVC : UIPickerViewDataSource {
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
                return pickOption.count
          
    }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
                return pickOption[row]
    }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
                DemandeTypeTF.text = pickOption[row]
            if row == 0 {
            
            API.globalPageViewControllerReference?.setViewControllers([(API.globalPageViewControllerReference?.tabViews[0])!], direction: .forward, animated: true, completion: nil)
            }
            if row == 1 {
                
                API.globalPageViewControllerReference?.setViewControllers([(API.globalPageViewControllerReference?.tabViews[1])!], direction: .forward, animated: true, completion: nil)
            }
            if row == 2 {
                
                API.globalPageViewControllerReference?.setViewControllers([(API.globalPageViewControllerReference?.tabViews[2])!], direction: .forward, animated: true, completion: nil)
            }

           
    }
        
}


