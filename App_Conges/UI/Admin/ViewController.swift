//
//  ViewController.swift
//  App_Conges
//
//  Created by Hamza on 8/8/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var slider: Slider!
    override func viewDidLoad() {
        super.viewDidLoad()
        slider.stepsCount = 26
slider.separatorImage = #imageLiteral(resourceName: "separator")
        slider.indicatorView.thumbImage = #imageLiteral(resourceName: "pointeurblue")
        // Do any additional setup after loading the view.
        slider.setTabOfPointeur(pointeurTab:[Pointeur(message: "12/2/2014", index: Int(8), poiteurImage: UIImage(named: "pointeuryellow")!) ,Pointeur(message: "5/12/2014", index: 11, poiteurImage: UIImage(named: "pointeurgreen")!), Pointeur(message: "12/2/2002", index: 19, poiteurImage: UIImage(named: "pointeurred")!)],setepsCount:26)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
