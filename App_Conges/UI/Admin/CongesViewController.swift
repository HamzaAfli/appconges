//
//  CongesViewController.swift
//  App_Conges
//
//  Created by Hamza on 8/11/17.
//  Copyright © 2017 Hamza. All rights reserved.
//


    import UIKit
    import MGSwipeTableCell
  
    import SCLAlertView

    class CongesViewController: ViewController  {
        //MARK:- Outlet
        @IBOutlet weak var profileImg: RoundImage!
        @IBOutlet weak var tabView: UITableView!
        @IBOutlet weak var searchTF: UITextField!
        
        @IBOutlet weak var departementTF: UITextField!
        
        @IBOutlet weak var nbrDemandeLBL: UILabel!
        @IBOutlet weak var tousLesSalariesTF: UITextField!
        @IBOutlet weak var enAttenteTF: UITextField!
        @IBOutlet weak var toutLesAnneesTF: UITextField!
        //MARK:- Variable
        var tableInfo = Array<Autorisation>()
        var tableInfoSearch = Array<Autorisation>()
        var cellTablesearch = Array<DemendeCongesCell>()
        var celltab = Array<DemendeCongesCell>()
        var pickCategorieOption = ["year1","year2"]
        let pickerViewCategorie = UIPickerView()

        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.startLoading(message: "Telechargement...")
            searchTF.rightViewMode = .always
            searchTF.rightView = UIImageView(image: #imageLiteral(resourceName: "searcher"))
            
            departementTF.rightViewMode = .always
            departementTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            tousLesSalariesTF.rightViewMode = .always
            tousLesSalariesTF.rightView = UIImageView(image:  #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            enAttenteTF.rightViewMode = .always
            enAttenteTF.rightView = UIImageView(image:  #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            toutLesAnneesTF.rightViewMode = .always
            toutLesAnneesTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            profileImg.awakeFromNib()
            tabView.delegate = self
            tabView.dataSource = self
            pickerViewCategorie.delegate = self
            toutLesAnneesTF.inputView = pickerViewCategorie
            API.getYears(onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                if succes {
                    if json?["success"] as! Bool {
                        let data = Json4Swift_Years(dictionary: json! as NSDictionary)
                        API.years = (data?.body)!
                        self.pickCategorieOption.removeAll()
                        for cat in (API.years){
                            self.pickCategorieOption.append(String(describing:cat))
                            self.pickerViewCategorie.reloadAllComponents()
                            
                        }
                        
                    }else{
                        SCLAlertView().showTitle(
                            "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                        
                        
                    }
                }else{                }
                
            })
            API.loadAllTransaction(onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                if succes {
                    if json?["success"] as! Bool {
                      
                        API.transactionData = Json4Swift_Transaction(dictionary: json! as NSDictionary)
                        self.nbrDemandeLBL.text = String(describing: API.transactionData!.body!.transactions!.count)
                        for tr in (API.transactionData?.body?.transactions)! {
                            let info = Autorisation()
                            info.name = tr.employeeUsername!
                            info.dure = tr.categoryName!
                            info.date = tr.eventDate!
                            info.etat = tr.status!
                            info.periode = "\(tr.beginDate!)--\(tr.endDate!)"
                            info.recuperation = "Nombre de jours:\(tr.totalDays!)jour(s)"
                            info.img = #imageLiteral(resourceName: "profile")
                            info.id = tr.id
                            self.tableInfo.append(info)
                        }
                        for info in self.tableInfo {
                            let cell = self.tabView.dequeueReusableCell(withIdentifier:"DemendeCongesCell") as! DemendeCongesCell
                            cell.nameLBL.text = info.name!
                            cell.img.image = info.img!
                            cell.dateLBL.text = info.date!
                            cell.dureLBL.text = info.dure!
                            cell.etatLBL.text = info.etat
                            cell.periodeLBL.text = info.periode
                            cell.recuperationLBL.text = info.recuperation!
                            
                            let faveButton = FaveButton(
                                frame: CGRect(x:0, y:0, width: 44, height: 44),
                                faveIconNormal: #imageLiteral(resourceName: "thumbsUpGreen")
                            )
                            let faveButton2 = FaveButton(
                                frame: CGRect(x:0, y:0, width: 44, height: 44),
                                faveIconNormal: #imageLiteral(resourceName: "dislikeRed")
                            )
                            
                            cell.rightButtons = [ MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "dislike-thumb"), backgroundColor: .red){
                                (sender: MGSwipeTableCell!) -> Bool in
                                faveButton.removeFromSuperview()
                                faveButton2.delegate = self
                                faveButton2.dotFirstColor = UIColor.green
                                faveButton2.dotSecondColor = UIColor.red
                                faveButton2.circleFromColor = UIColor.blue
                                faveButton2.circleToColor = UIColor.yellow
                                
                                
                                
                                faveButton2.selectedColor = UIColor.init(hex: "FF0000")
                                let id = self.tableInfo[(cell.indexPath?.row)!].id
                                
                                API.cancelTransaction(id: id!, onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                                    if succes {
                                        if json?["success"] as! Bool {
                                            cell.favButtonView.addSubview(faveButton2)
                                            faveButton2.animateSelect(true, duration: 6)
                                            cell.favButtonView.isUserInteractionEnabled = false
                                           
                                                
                                            
                                            
                                        }else{
                                            SCLAlertView().showTitle(
                                                "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                                            
                                            
                                        }
                                    }else{
                                        SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                                    
                                    }
                                    
                                })
                                
                                

                                 return true
                            
                                },MGSwipeButton(title: "",icon: #imageLiteral(resourceName: "thumb-up"), backgroundColor: UIColor.green) {
                                    (sender: MGSwipeTableCell!) -> Bool in
                                    faveButton2.removeFromSuperview()
                                    faveButton.delegate = self
                                    faveButton.dotFirstColor = UIColor.green
                                    faveButton.dotSecondColor = UIColor.red
                                    faveButton.circleFromColor = UIColor.blue
                                    faveButton.circleToColor = UIColor.yellow
                                    faveButton.selectedColor = UIColor.green
                                    
                                    
                                    let id = self.tableInfo[(cell.indexPath?.row)!].id
                                    
                                    API.acceptTransaction(id: id!, onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                                        if succes {
                                            if json?["success"] as! Bool {
                                                cell.favButtonView.addSubview(faveButton)
                                                faveButton.animateSelect(true, duration: 6)
                                                cell.favButtonView.isUserInteractionEnabled = false
                                                
                                                
                                                
                                                
                                            }else{
                                                SCLAlertView().showTitle(
                                                    "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                                                
                                                
                                            }
                                        }else{
                                            SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                                            
                                        }
                                        
                                    })
                                   
                                    return true
                                },MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "pencil"), backgroundColor: .blue) {
                                    (sender: MGSwipeTableCell!) -> Bool in
                                    API.transaction = (API.transactionData?.body?.transactions?[(cell.indexPath?.row)!])!
                                    API.adminSlideVCReference?.changeViewController(menu: .miseAJourConges)
                                    return true
                                }]
                            
                            cell.rightSwipeSettings.transition = .rotate3D
                            
                            
                            self.celltab.append(cell)
                            
                        }
                        
                        self.cellTablesearch = self.celltab
                        self.tableInfoSearch = self.tableInfo
                        self.tabView.reloadData()
                        
                        self.stopLoading(succes: true)
                    }else{
                        self.stopLoading(succes: false)
                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (t:Timer) in
                        SCLAlertView().showTitle(
                            "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                        
                         })
                    }
                }else{
                  
                    self.stopLoading(succes: false)
                    
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (t:Timer) in
                        SCLAlertView().showWarning("", subTitle: "Network Connection Failed !!")
                    })
                   
                }
                
                
                
            })
            
            
         
            
            
            
            
            searchTF.addTarget(self, action: #selector(searchWithName(_ :)), for: .editingChanged)
            
            
            
        }
        
        override func viewDidLayoutSubviews() {
            profileImg.awakeFromNib()
        }
        override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
            super.viewWillTransition(to: size, with: coordinator)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.setNavigationBarItem()
            
            
        }
        
        
        func searchWithName(_ textfield:UITextField){
            
            cellTablesearch.removeAll()
            tableInfoSearch.removeAll()
            if textfield.text?.characters.count != 0 {
                for str in tableInfo {
                    let range = str.name.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        tableInfoSearch.append(str)
                    }
                }
                
                for str in celltab {
                    let range = str.nameLBL.text!.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                    
                    if range != nil {
                        cellTablesearch.append(str)
                    }
                }
            } else {
                tableInfoSearch = tableInfo
                cellTablesearch = celltab
            }
            
            tabView.reloadData()
            
            
        }
    }
    //MARK:-  Extension
    
    //MARK: TableView
    
    extension CongesViewController:UITableViewDelegate{
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
    }
    
    extension CongesViewController:UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tableInfoSearch.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            cellTablesearch[indexPath.row].indexPath = indexPath
            
            return cellTablesearch[indexPath.row]
            
            
        }
        
        
    }
    
    
    //Mark:- SlideMenu
    extension CongesViewController : SlideMenuControllerDelegate {
        
        func leftWillOpen() {
            print("SlideMenuControllerDelegate: leftWillOpen")
        }
        
        func leftDidOpen() {
            print("SlideMenuControllerDelegate: leftDidOpen")
        }
        
        func leftWillClose() {
            print("SlideMenuControllerDelegate: leftWillClose")
        }
        
        func leftDidClose() {
            print("SlideMenuControllerDelegate: leftDidClose")
        }
        
        func rightWillOpen() {
            print("SlideMenuControllerDelegate: rightWillOpen")
        }
        
        func rightDidOpen() {
            print("SlideMenuControllerDelegate: rightDidOpen")
        }
        
        func rightWillClose() {
            print("SlideMenuControllerDelegate: rightWillClose")
        }
        
        func rightDidClose() {
            print("SlideMenuControllerDelegate: rightDidClose")
        }
        
        
        
}


//MARK:PickerView
extension CongesViewController : UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
}
extension CongesViewController : UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickCategorieOption.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickCategorieOption[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        toutLesAnneesTF.text = pickCategorieOption[row]
        
    }
}


