//
//  DemendeCongesCell.swift
//  App_Conges
//
//  Created by Hamza on 8/13/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class DemendeCongesCell: MGSwipeTableCell {

    @IBOutlet weak var recuperationLBL: UILabel!
    @IBOutlet weak var periodeLBL: UILabel!
    @IBOutlet weak var dureLBL: UILabel!
    
    @IBOutlet weak var img: RoundImage!
    
    @IBOutlet weak var nameLBL: UILabel!
    
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var etatLBL: UILabel!
    @IBOutlet weak var favButtonView: UIView!
    var indexPath:IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
