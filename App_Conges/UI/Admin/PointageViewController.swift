//
//  PointageViewController.swift
//  App_Conges
//
//  Created by Hamza on 8/7/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import SCLAlertView
//import PSPDFKit
import PDFGenerator


class PointageViewController: ViewController  {
    //MARK:- Outlet
    @IBOutlet weak var profileImg: RoundImage!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var departementTF: UITextField!
    
    @IBOutlet weak var dateFinTF: UITextField!
    @IBOutlet weak var dateDebTF: UITextField!
    @IBOutlet weak var toutLesSalariesTF: UITextField!
    //MARK:- Variable
    var tableInfo = Array<PointageUserInfo>()
    var tableInfoSearch = Array<PointageUserInfo>()
    var cellTablesearch = Array<PointageCell>()
    var celltab = Array<PointageCell>()
    var dateDeb : Date?
    var dateFin :Date?

    
    
       override func viewDidLoad() {
             super.viewDidLoad()
       
        searchTF.rightViewMode = .always
        searchTF.rightView = UIImageView(image: #imageLiteral(resourceName: "searcher"))
        
        departementTF.rightViewMode = .always
        departementTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
        dateFinTF.rightViewMode = .always
        dateFinTF.rightView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
        dateDebTF.rightViewMode = .always
        dateDebTF.rightView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
        toutLesSalariesTF.rightViewMode = .always
        toutLesSalariesTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
           profileImg.awakeFromNib()
        tabView.delegate = self
        tabView.dataSource = self
        for i in 1...4{
            
            var tabPointeur = [Pointeur(message: "1/2/2014", index: Int(0), poiteurImage: #imageLiteral(resourceName: "pointeurviolet")) ,Pointeur(message: "5/2/2014", index:  Int(10), poiteurImage: UIImage(named: "pointeurgreen")!), Pointeur(message: "7/2/2012", index:  Int(20), poiteurImage: UIImage(named: "pointeurblue")!)]
            tableInfo.append(PointageUserInfo(profileImg: #imageLiteral(resourceName: "profile"), name: "med amine", matricule: "0037+\(i)", tabpointeurs: tabPointeur, total: "12:00:10", backgroundSliderColor: UIColor.red))
            
            tabPointeur = [Pointeur(message: "12/2/2014", index: Int(0), poiteurImage: UIImage(named: "pointeuryellow")!) ,Pointeur(message: "5/12/2014", index:  Int(5), poiteurImage: UIImage(named: "pointeurgreen")!), Pointeur(message: "12/2/2002", index:  Int(16), poiteurImage: UIImage(named: "pointeurred")!)]
        tableInfo.append(PointageUserInfo(profileImg: #imageLiteral(resourceName: "profile"), name: "amine", matricule: "0012+\(i)", tabpointeurs: tabPointeur, total: "8:00:10", backgroundSliderColor: UIColor.blue))
        
         tabPointeur = [Pointeur(message: "1/2/2014", index: Int(23), poiteurImage: UIImage(named: "pointeurgreen")!) ,Pointeur(message: "5/2/2014", index:  Int(15), poiteurImage: UIImage(named: "pointeurgreen")!), Pointeur(message: "7/2/2012", index:  Int(1), poiteurImage: UIImage(named: "pointeurblue")!)]
        tableInfo.append(PointageUserInfo(profileImg: #imageLiteral(resourceName: "profile"), name: "hamza", matricule: "0030+\(i)", tabpointeurs: tabPointeur, total: "7:00:10", backgroundSliderColor: UIColor.green))
        
        tabPointeur = [Pointeur(message: "1/2/2014", index: Int(23), poiteurImage: #imageLiteral(resourceName: "pointeurred")) ,Pointeur(message: "5/2/2014", index:  Int(15), poiteurImage: UIImage(named: "pointeurgreen")!), Pointeur(message: "7/2/2012", index:  Int(1), poiteurImage: UIImage(named: "pointeurblue")!)]
        tableInfo.append(PointageUserInfo(profileImg: #imageLiteral(resourceName: "profile"), name: "hamza afli", matricule: "0060+\(i)", tabpointeurs: tabPointeur, total: "9:00:10", backgroundSliderColor: UIColor.cyan))
        tabPointeur = [Pointeur(message: "1/2/2014", index: Int(0), poiteurImage: #imageLiteral(resourceName: "pointeurviolet")) ,Pointeur(message: "5/2/2014", index:  Int(10), poiteurImage: UIImage(named: "pointeurgreen")!), Pointeur(message: "7/2/2012", index:  Int(20), poiteurImage: UIImage(named: "pointeurblue")!)]
        tableInfo.append(PointageUserInfo(profileImg: #imageLiteral(resourceName: "profile"), name: "med amine", matricule: "0037+\(i)", tabpointeurs: tabPointeur, total: "12:00:10", backgroundSliderColor: UIColor.yellow))
        
            
           
        }
        
        
        
        tableInfoSearch = tableInfo
       
        
        searchTF.addTarget(self, action: #selector(searchWithName(_ :)), for: .editingChanged)
        
        for info in tableInfo {
        let cell = tabView.dequeueReusableCell(withIdentifier:"PointageCell") as! PointageCell
        cell.slider.setTabOfPointeur(pointeurTab:info.tabpointeurs,setepsCount:26,view:cell.viewColore,color:info.backgroundSliderColor)
        
        
        cell.nameLbl.text = info.name
        cell.matricule.text = info.matricule
        cell.img.image = info.profileImg
                    
            
        celltab.append(cell)
            
        }
        cellTablesearch = celltab
    }

    override func viewDidLayoutSubviews() {
        profileImg.awakeFromNib()
           }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
      
        
    }
    //MARK:- Action
    
    
    @IBAction func filtrerPressed(_ sender: Any) {
       
    }
    
    @IBAction func exporterPressed(_ sender: Any) {
 var documentController : UIDocumentInteractionController!
        
        
        
        let dst = URL(fileURLWithPath:"Document.pdf")
        // outputs as Data
        do {
            let data = try PDFGenerator.generated(by: [tabView])
           try data.write(to: (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!).appendingPathComponent("Document.pdf"), options: .atomic)
        } catch (let error) {
            print(error)
        }
        
        // writes to Disk directly.
        do {
            try PDFGenerator.generate([tabView], to: dst)
        } catch (let error) {
            print(error)
        }
        documentController = UIDocumentInteractionController(url:(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!).appendingPathComponent("Document.pdf"))
        documentController.presentOptionsMenu(from: (sender as! UIButton).frame, in: self.view, animated: true)


//
//        let fileURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!)//Bundle.main.url(forResource: "Document", withExtension: "pdf")!
//        let document = PSPDFDocument(url: fileURL)
//        
//        let pdfController = PSPDFViewController(document: document, configuration: PSPDFConfiguration.default()) //PSPDFScrollView(document:document)
//        
//        present(UINavigationController(rootViewController: pdfController), animated: true)
//        
//        
//        
//        UIApplication.shared.open(URL(string: "http://\(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!)")!)
//        
//    
//    /*
//    
//        let dst = URL(fileURLWithPath:"Document.pdf")
//        // outputs as Data
//        do {
//            let data = try PDFGenerator.generated(by: [tabView])
//            try data.write(to: (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!).appendingPathComponent("Document.pdf"), options: .atomic)
//        } catch (let error) {
//            print(error)
//        }
//        
//        // writes to Disk directly.
//        do {
//            try PDFGenerator.generate([tabView], to: dst)
//        } catch (let error) {
//            print(error)
//        }
//
//    
//    
//        let fileManager = FileManager.default
//        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
//        if let documentDirectory: NSURL = urls.first as! NSURL  {
//            // This is where the database should be in the documents directory
//            let finalDatabaseURL = documentDirectory.appendingPathComponent("Document.pdf")
//            
//          /*  if  (finalDatabaseURL?.checkResourceIsReachable())! {
//                // The file already exists, so just return the URL
//                return finalDatabaseURL
//            } else {*/
//                // Copy the initial file from the application bundle to the documents directory
//                if let bundleURL = Bundle.main.url(forResource: "Document", withExtension: "pdf") {
//                    do {
//                        try fileManager.copyItem(at: bundleURL, to: finalDatabaseURL!)
//                 
//                        return  UIApplication.shared.open(URL(string: "com.adobe.adobe-reader://\(String(describing: finalDatabaseURL))")!)
//                    } catch (let error) {
//                    print(error)
//                    }
//                    
//                  
//                } else {
//                    print("Couldn't find initial database in the bundle!")
//                }
//           // }
//        } else {
//            print("Couldn't get documents directory!")
//        }
//        
// 
//    */
//    
//    
    
    }
    
    
    
    @IBAction func dateDebTFEditing(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        var components = DateComponents()
        components.year = 0
        components.month = 0
        components.day = 0
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = 100
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChangedForDateDebTF), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func dateFinTFEditing(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        var components = DateComponents()
        components.year = 0
        components.month = 0
        components.day = 0
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = 100
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChangedForDateFinTF), for: UIControlEvents.valueChanged)
    }
    
    
    @IBAction func dateFinEditingdidEnd(_ sender: UITextField) {
        if dateDebTF.text != "" && dateFinTF.text != ""{
            
            if compareDate(date1: dateDeb!, date2: dateFin!) {
                SCLAlertView().showTitle(
                    "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                dateDebTF.text = ""
                dateFinTF.text = ""
            }
        }
    }
    @IBAction func datedebEditingDidEnd(_ sender: UITextField) {
        if dateFinTF.text != "" && dateDebTF.text != "" {
                        if compareDate(date1: dateDeb!, date2: dateFin!) {
                SCLAlertView().showTitle(
                    "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                dateDebTF.text = ""
                dateFinTF.text = ""
            }
        }
        
    }

    
    
//MARK:- Function
    
    func searchWithName(_ textfield:UITextField){
        
        cellTablesearch.removeAll()
        tableInfoSearch.removeAll()
        if textfield.text?.characters.count != 0 {
            for str in tableInfo {
                let range = str.name.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                
                if range != nil {
                    tableInfoSearch.append(str)
                }
            }
            
            for str in celltab {
                let range = str.nameLbl.text!.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                
                if range != nil {
                    cellTablesearch.append(str)
                }
            }
        } else {
            tableInfoSearch = tableInfo
            cellTablesearch = celltab
        }
        
        tabView.reloadData()

    
       }
    
    @objc private func datePickerValueChangedForDateDebTF(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        dateDeb = sender.date
        
        dateFormatter.dateFormat = "dd/MMM/yyyy"
        dateDebTF.text = dateFormatter.string(from: sender.date)
        
    }
    
    @objc private func datePickerValueChangedForDateFinTF(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFin = sender.date
        
        dateFormatter.dateFormat = "dd/MMM/yyyy"
        dateFinTF.text = dateFormatter.string(from: sender.date)
        
    }
}
//MARK:-  Extension

//MARK: TableView

extension PointageViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension PointageViewController:UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableInfoSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return cellTablesearch[indexPath.row]
       
        
    }
    

}


//Mark:- SlideMenu
extension PointageViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
    
    
    
}





