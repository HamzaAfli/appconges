//
//  PointageUserInfo.swift
//  App_Conges
//
//  Created by Hamza on 8/9/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
import UIKit
class  PointageUserInfo{
    var profileImg : UIImage!
    var name:String!
    var matricule:String!
    var tabpointeurs:[Pointeur]!
    var total:String!
    var backgroundSliderColor:UIColor!

    init(profileImg : UIImage!,name:String!,matricule:String!,tabpointeurs:[Pointeur]!,total:String!,backgroundSliderColor:UIColor!) {
        self.profileImg = profileImg
        self.name = name
        self.matricule = matricule
        self.tabpointeurs = tabpointeurs
        self.total = total
        self.backgroundSliderColor = backgroundSliderColor
    }
}
