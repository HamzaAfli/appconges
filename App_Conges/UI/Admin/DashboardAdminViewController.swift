//
//  DashboardAdminViewController.swift
//  App_Conges
//
//  Created by Hamza on 8/9/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class DashboardAdminViewController: ViewController {
    //MARK: Outlet
    
    
    @IBOutlet weak var profileImg: RoundImage!
    
    
    @IBOutlet weak var heurDeTravialLbl: UILabel!
    
    
    @IBOutlet weak var noteMoyLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var roleLbl: UILabel!
    
    @IBOutlet weak var joursTravailésLbl: UILabel!
    @IBOutlet weak var heurestravailesLbl: UILabel!
    
    @IBOutlet weak var congesView: RoundView!
    
    @IBOutlet weak var retardLbl: UILabel!
    
    @IBOutlet weak var congesLbl: UILabel!
    
    @IBOutlet weak var autorisationLbl: UILabel!
    
    @IBOutlet weak var deplacementLbl: UILabel!
    @IBOutlet weak var deplacementView: RoundView!
    @IBOutlet weak var autorisationView: RoundView!
    @IBOutlet weak var nbrDemandeLbl: UILabel!
//    @IBOutlet weak var profileImg: RoundImage!
//    @IBOutlet weak var heurDeTravialLbl: UILabel!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var roleLbl: UILabel!
//    @IBOutlet weak var joursTravailésLbl: UILabel!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var : RoundView!
//    @IBOutlet weak var : AddBtn!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var : RoundView!
//    @IBOutlet weak var : RoundView!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var : UILabel!
//    @IBOutlet weak var : UILabel!
    //MARK: Variable
    
    
    override func viewDidLoad() {
       super.viewDidLoad()
     
        
        congesLbl.text = "90"
        autorisationLbl.text = "50"
        deplacementLbl.text = "100"
        congesView.makeProgressLine(value: congesLbl.text!)
        deplacementView.makeProgressLine(value: deplacementLbl.text!)
       autorisationView.makeProgressLine(value: autorisationLbl.text!)
        fillDashboardwithData()
       
    }
    
    deinit {
    }
    override func viewDidLayoutSubviews() {
       profileImg.awakeFromNib()

        congesLbl.awakeFromNib()
        deplacementLbl.awakeFromNib()
        autorisationLbl.awakeFromNib()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "DASHBOARD"
        
    }
    
    //Mark:-Action
    @IBAction func AddPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)


        let nouvelleDemandeVC = storyboard.instantiateViewController(withIdentifier: "NouvelleDemandeVC") as! NouvelleDemandeVC
        SlideVC.s_isDashBoardVC = false
        SlideVC.s_isNouvelleDemandeVC = true
        let nvc: UINavigationController = UINavigationController(rootViewController: nouvelleDemandeVC)
        
        
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = true
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barStyle = UIBarStyle.black
        self.slideMenuController()?.changeMainViewController(nvc, close: true)
        
    }
    
    //Mark:-Private Function
    private func fillDashboardwithData(){
        nameLbl.text = API.data?.username!
        heurDeTravialLbl.text = String(describing: API.data!.employee!.hmTravail!)
        noteMoyLbl.text = String(describing: API.data!.employee!.noteMoyenne!)
        heurestravailesLbl.text = String(describing: API.data!.employee!.heureTravaille!)
        joursTravailésLbl.text = String(describing: API.data!.employee!.joursTravaille!)
        retardLbl.text = String(describing: API.data!.employee!.retard!)
        nbrDemandeLbl.text = String(describing: API.data!.employee!.solde!)

       
    }
    
    
}


//Mark:-Extension
extension DashboardAdminViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
    
    
    
}
