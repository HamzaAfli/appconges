//
//  AdminSlideVC.swift
//  App_Conges
//
//  Created by Hamza on 8/10/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

import SCLAlertView
enum LeftMenu2: Int {
    case dashBoard = 0
    case nouvelleDemande
    case joursFerier
    case conges
    case deplacement
    case pointage
    case miseAJourConges
    case miseAJourDeplacement
    case miseAjourAutorisation
    case logout
}

class AdminSlideVC: ViewController {
    
    //MARK: Static
    static  var  s_isDashBoardVC = true
    static  var  s_isNouvelleDemandeVC = false
    static  var  s_isJoursFeriesViewController = false
    
    //MARK: Outlet
    @IBOutlet var tabView: UITableView!
    @IBOutlet weak var profileImg: RoundImage!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var societeLbl: UILabel!
    //MARK: Variable
    var dashBoard:UINavigationController!
    var nouvelleDemande:UINavigationController!
    var joursFerierVC:UINavigationController!
    var conges:UINavigationController!
    var deplacement:UINavigationController!
    var pointage:UINavigationController!
    var miseAJourConges:UINavigationController!
    var miseAJourDeplacement:UINavigationController!
    var miseAjourAutorisation:UINavigationController!
    var logout:UINavigationController!
    
    let imgTab = [#imageLiteral(resourceName: "dashboard_icn"),#imageLiteral(resourceName: "plus"),#imageLiteral(resourceName: "jrsFeries_icn"),#imageLiteral(resourceName: "conger_icon"),#imageLiteral(resourceName: "deplacement_icon"),#imageLiteral(resourceName: "pointage_icon"),#imageLiteral(resourceName: "miseAjours_icon"),#imageLiteral(resourceName: "miseAjours_icon"),#imageLiteral(resourceName: "miseAjours_icon"),#imageLiteral(resourceName: "logout")]
    let lblTxtTab = ["DASHBOARD","NOUVELLE DEMANDE","JOURS FERIÉS","CONGÉS","DEPLACEMENT","POINTAGE","CONGÉS","DEPLACEMENT","AUTORISATION","LOGOUT"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        API.adminSlideVCReference = self
        
        tabView.dataSource = self
        tabView.delegate = self
        
        
        
        
    }
    override func viewDidLayoutSubviews() {
        profileImg.awakeFromNib()
    }
    
    func changeViewController(menu : LeftMenu2){
        switch menu {
        case .dashBoard:
            
            let dash =  storyboard?.instantiateViewController(withIdentifier: "DashboardAdminViewController") as! DashboardAdminViewController
            self.dashBoard = UINavigationController(rootViewController: dash)
            
            dashBoard.navigationBar.setBackgroundImage(UIImage(), for: .default)
            dashBoard.navigationBar.shadowImage = UIImage()
            dashBoard.navigationBar.isTranslucent = true
            dashBoard.navigationBar.tintColor = UIColor.white
            dashBoard.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(dashBoard, close: true)
            
        case .nouvelleDemande:
            let nv =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NouvelleDemandeVC") as! NouvelleDemandeVC
            self.nouvelleDemande = UINavigationController(rootViewController: nv)
            nouvelleDemande.navigationBar.setBackgroundImage(UIImage(), for: .default)
            nouvelleDemande.navigationBar.shadowImage = UIImage()
            nouvelleDemande.navigationBar.isTranslucent = true
            nouvelleDemande.navigationBar.tintColor = UIColor.white
            nouvelleDemande.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(nouvelleDemande, close: true)
        case .joursFerier:
            let joursFerier =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JoursFeriesViewController") as! JoursFeriesViewController
            self.joursFerierVC = UINavigationController(rootViewController: joursFerier)
            joursFerierVC.navigationBar.setBackgroundImage(UIImage(), for: .default)
            joursFerierVC.navigationBar.shadowImage = UIImage()
            joursFerierVC.navigationBar.isTranslucent = true
            joursFerierVC.navigationBar.tintColor = UIColor.white
            joursFerierVC.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(joursFerierVC, close: true)
        case .conges:
            let nv =  storyboard?.instantiateViewController(withIdentifier: "CongesViewController") as! CongesViewController
            self.conges = UINavigationController(rootViewController: nv)
            conges.navigationBar.setBackgroundImage(UIImage(), for: .default)
            conges.navigationBar.shadowImage = UIImage()
            conges.navigationBar.isTranslucent = true
            conges.navigationBar.tintColor = UIColor.white
            conges.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(conges, close: true)
        case .deplacement:
            let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "DeplacementViewController") as! DeplacementViewController
            self.deplacement = UINavigationController(rootViewController: joursFerier)
            deplacement.navigationBar.setBackgroundImage(UIImage(), for: .default)
            deplacement.navigationBar.shadowImage = UIImage()
            deplacement.navigationBar.isTranslucent = true
            deplacement.navigationBar.tintColor = UIColor.white
            deplacement.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(deplacement, close: true)
            
        case .pointage:
            let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "PointageViewController") as! PointageViewController
            self.pointage = UINavigationController(rootViewController: joursFerier)
            pointage.navigationBar.setBackgroundImage(UIImage(), for: .default)
            pointage.navigationBar.shadowImage = UIImage()
            pointage.navigationBar.isTranslucent = true
            pointage.navigationBar.tintColor = UIColor.white
            pointage.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(pointage, close: true)
        case .miseAJourConges:
            let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "MiseAJourCongesVC") as! MiseAJourCongesVC
            self.miseAJourConges = UINavigationController(rootViewController: joursFerier)
            miseAJourConges.navigationBar.setBackgroundImage(UIImage(), for: .default)
            miseAJourConges.navigationBar.shadowImage = UIImage()
            miseAJourConges.navigationBar.isTranslucent = true
            miseAJourConges.navigationBar.tintColor = UIColor.white
            miseAJourConges.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(miseAJourConges, close: true)
        case .miseAJourDeplacement:
            let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "MiseAJourDeplacementVC") as! MiseAJourDeplacementVC
            self.miseAJourDeplacement = UINavigationController(rootViewController: joursFerier)
            miseAJourDeplacement.navigationBar.setBackgroundImage(UIImage(), for: .default)
            miseAJourDeplacement.navigationBar.shadowImage = UIImage()
            miseAJourDeplacement.navigationBar.isTranslucent = true
            miseAJourDeplacement.navigationBar.tintColor = UIColor.white
            miseAJourDeplacement.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(miseAJourDeplacement, close: true)
        case .miseAjourAutorisation:
            let joursFerier =  storyboard?.instantiateViewController(withIdentifier: "MiseAJourAutorisationVC") as! MiseAJourAutorisationVC
            self.miseAjourAutorisation = UINavigationController(rootViewController: joursFerier)
            miseAjourAutorisation.navigationBar.setBackgroundImage(UIImage(), for: .default)
            miseAjourAutorisation.navigationBar.shadowImage = UIImage()
            miseAjourAutorisation.navigationBar.isTranslucent = true
            miseAjourAutorisation.navigationBar.tintColor = UIColor.white
            miseAjourAutorisation.navigationBar.barStyle = UIBarStyle.black
            self.slideMenuController()?.changeMainViewController(miseAjourAutorisation, close: true)
            
 
        case .logout:
            
            API.logout(onComplete:{ (succes:Bool, json:[String : AnyObject]?) in
                if succes {
                    if json?["success"] as! Bool {
                         self.dismiss(animated: true, completion: nil)
                        //self.slideMenuController()?.changeMainViewController(self.logout, close: true)
                       UIApplication.shared.delegate?.window??.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                        
                    }else{
                        SCLAlertView().showTitle(
                            "",subTitle: json!["errors"] as! String,duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                        
                        
                    }
                }else{
                    SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                    
                }
                
            })
            
            
            
            
            
            
        }
        
    }
    
}

//MARK: Extension

//MARK: UITableView


extension AdminSlideVC:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
}

extension AdminSlideVC:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Compte"
        }else if section == 1 {
            return "administration"
            
        }else if section == 2 {
            return "Mise à jour"
            
        }else {return "Paramétres"}
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 3}
        else if section == 1 {return 3}
        else if section == 2{return 3}
        else {return 1}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabView.dequeueReusableCell(withIdentifier:"MenuCellAdmin") as! MenuCellAdmin
        if indexPath.section == 0 {
            
            cell.lbl.text = lblTxtTab[indexPath.row]
            cell.Img.image = imgTab[indexPath.row]
          
               
              
                
        
            
        }else if indexPath.section == 1 {
            cell.lbl.text = lblTxtTab[indexPath.row + 3]
            cell.Img.image = imgTab[indexPath.row + 3]
            
        }else if indexPath.section == 2 {
            cell.lbl.text = lblTxtTab[indexPath.row + 6]
            cell.Img.image = imgTab[indexPath.row + 6]
            
        }else {
            cell.lbl.text = lblTxtTab[9]
            cell.Img.image = imgTab[9]

        
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {

            if let menu = LeftMenu2(rawValue: indexPath.row){
                self.changeViewController(menu: menu)
                }
            }
            if indexPath.section == 1 {
                if let menu = LeftMenu2(rawValue: indexPath.row+3){
                    self.changeViewController(menu: menu)
                }
                
            }
            if indexPath.section == 2 {
                if let menu = LeftMenu2(rawValue: indexPath.row+6){
                    self.changeViewController(menu: menu)
                }
                
            }
        if indexPath.section == 3 {
            if let menu = LeftMenu2(rawValue: indexPath.row+9){
                self.changeViewController(menu: menu)
            }
            
        }
        
        
        }
}


    

