//
//  DeplacementViewController.swift
//  App_Conges
//
//  Created by Hamza on 8/11/17.
//  Copyright © 2017 Hamza. All rights reserved.
//



import UIKit
import MGSwipeTableCell
import SCLAlertView

class  DeplacementViewController: ViewController  {
    //MARK:- Outlet
    @IBOutlet weak var profileImg: RoundImage!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var departementTF: UITextField!
    
    @IBOutlet weak var tousLesSalariesTF: UITextField!
    @IBOutlet weak var enAttenteTF: UITextField!
    @IBOutlet weak var toutLesAnneesTF: UITextField!
    //MARK:- Variable
    var tableInfo = Array<Autorisation>()
    var tableInfoSearch = Array<Autorisation>()
    var cellTablesearch = Array<DemendeCongesCell>()
    var celltab = Array<DemendeCongesCell>()
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        searchTF.rightViewMode = .always
        searchTF.rightView = UIImageView(image: #imageLiteral(resourceName: "searcher"))
        
        departementTF.rightViewMode = .always
        departementTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
        tousLesSalariesTF.rightViewMode = .always
        tousLesSalariesTF.rightView = UIImageView(image:  #imageLiteral(resourceName: "arrow-point-to-down-Black"))
        enAttenteTF.rightViewMode = .always
        enAttenteTF.rightView = UIImageView(image:  #imageLiteral(resourceName: "arrow-point-to-down-Black"))
        toutLesAnneesTF.rightViewMode = .always
        toutLesAnneesTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
        profileImg.awakeFromNib()
        tabView.delegate = self
        tabView.dataSource = self
        for _ in 1...10{
            
            tableInfo.append(Autorisation(img: #imageLiteral(resourceName: "profile"), name: "Hamza", date: "12/12/2012", etat: "En attente", periode: "13/12/2012-15/12/2012", dure: "30 minute",recuperation:"15/06/2014",id:0))
            
            tableInfo.append(Autorisation(img: #imageLiteral(resourceName: "profile"), name: "med amine", date: "12/12/2012", etat: "En attente", periode: "13/12/2012-15/12/2012", dure: "30 minute",recuperation:"15/06/2014",id:0))
            
            
            tableInfo.append(Autorisation(img: #imageLiteral(resourceName: "profile"), name: "amine", date: "12/12/2012", etat: "En attente", periode: "13/12/2012-15/12/2012", dure: "30 minute",recuperation:"15/06/2014",id:0))
            
            
            tableInfo.append(Autorisation(img: #imageLiteral(resourceName: "profile"), name: "Hamza Afli", date: "12/12/2012", etat: "En attente", periode: "13/12/2012-15/12/2012", dure: "30 minute",recuperation:"15/06/2014",id:0))
            
            
            
        }
        
        
        
        tableInfoSearch = tableInfo
        
        
        searchTF.addTarget(self, action: #selector(searchWithName(_ :)), for: .editingChanged)
        
        for info in  tableInfo {
            
            let cell = tabView.dequeueReusableCell(withIdentifier:"DemendeCongesCell") as! DemendeCongesCell
            cell.nameLBL.text = info.name
            cell.img.image = info.img
            cell.dateLBL.text = info.date
            cell.dureLBL.text = "Durée:\(info.dure!)"
            cell.etatLBL.text = info.etat
            cell.periodeLBL.text = info.periode
            cell.recuperationLBL.text = "Recuperation:\(info.recuperation!)"
            
            
            
            cell.rightButtons = [ MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "trush"), backgroundColor: UIColor.darkGray){
                (sender: MGSwipeTableCell!) -> Bool in
                 let alert =  SCLAlertView()
              
                alert.addButton("confirmer", action: {
                    self.tableInfo.remove(at:(cell.indexPath?.row)!)
                    self.tableInfoSearch.remove(at: (cell.indexPath?.row)!)
                    self.celltab.remove(at:(cell.indexPath?.row)!)
                      self.cellTablesearch.remove(at:(cell.indexPath?.row)!)
                    self.tabView.reloadData()
                    
                })
            
                alert.showCustom("Supprimer", subTitle: "", color: UIColor.red, icon: #imageLiteral(resourceName: "trush"), closeButtonTitle: "Annuler", colorStyle:1, colorTextButton: 1, circleIconImage: #imageLiteral(resourceName: "trush"), animationStyle: SCLAnimationStyle.topToBottom)
                return true
                },MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "dislike-thumb"), backgroundColor: .red),MGSwipeButton(title: "",icon: #imageLiteral(resourceName: "closed"), backgroundColor: UIColor.init(hex: "a249d6")),MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "pencil"), backgroundColor: .cyan){
                (sender: MGSwipeTableCell!) -> Bool in
                API.adminSlideVCReference?.changeViewController(menu: .miseAJourDeplacement
                )
                return true
                }]
            cell.rightSwipeSettings.transition = .rotate3D
            
            
            celltab.append(cell)
            
        }
        cellTablesearch = celltab
    }
    
    override func viewDidLayoutSubviews() {
        profileImg.awakeFromNib()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        
        
    }
    
    
    func searchWithName(_ textfield:UITextField){
        
        cellTablesearch.removeAll()
        tableInfoSearch.removeAll()
        if textfield.text?.characters.count != 0 {
            for str in tableInfo {
                let range = str.name.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                
                if range != nil {
                    tableInfoSearch.append(str)
                }
            }
            
            for str in celltab {
                let range = str.nameLBL.text!.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil,   locale: nil)
                
                if range != nil {
                    cellTablesearch.append(str)
                }
            }
        } else {
            tableInfoSearch = tableInfo
            cellTablesearch = celltab
        }
        
        tabView.reloadData()
        
        
    }
}
//MARK:-  Extension

//MARK: TableView

extension  DeplacementViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension  DeplacementViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableInfoSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cellTablesearch[indexPath.row].indexPath = indexPath

        return cellTablesearch[indexPath.row]
        
        
    }
    
    
}


//Mark:- SlideMenu
extension  DeplacementViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
    
    
    
}




