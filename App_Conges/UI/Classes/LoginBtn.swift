//
//  LoginBtn.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/17/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class LoginBtn: UIButton {
    
    @IBInspectable open var backgroundLoginBtn : UIColor = UIColor(colorLiteralRed: 153/255, green: 179/255, blue: 54/255, alpha: 1)
    @IBInspectable open var borderColor :UIColor = UIColor.white

    
    override func awakeFromNib() {
        
        self.layer.backgroundColor = backgroundLoginBtn.cgColor
        self.layer.cornerRadius = self.frame.height/2.0
        self.layer.borderWidth = 2
        self.layer.borderColor = borderColor.cgColor
    
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
