//
//  RoundImage.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/20/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class RoundImage: UIImageView {

    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.width/2.0
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(colorLiteralRed: 106/255, green: 77/255, blue: 124/255, alpha: 1).cgColor
    }


}
