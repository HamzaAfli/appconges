//
//  JoursFerierCell.swift
//  App_Conges
//
//  Created by Hamza on 7/26/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class JoursFerierCell: UITableViewCell {

    
   // MARK:- Outlet
    
    @IBOutlet weak var rectImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
