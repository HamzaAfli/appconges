//
//  Slider.swift
//  SmartLumbarBelt
//
//  Created by Houcem soued on 13/01/2017.
//  Copyright © 2017 Thuasne. All rights reserved.
//

import UIKit

///  This a heighly customizable slider component
//@IBDesignable
open class Slider: UIControl {
    var test : Bool = true {
        didSet {
        }
    }
    // MARK: Constants
    fileprivate struct Animation {
        fileprivate static let withBounceDuration: TimeInterval = 0.3
        fileprivate static let springDamping: CGFloat = 0.75
        fileprivate static let withoutBounceDuration: TimeInterval = 0.2
    }
    fileprivate struct Color {
        fileprivate static let background: UIColor = UIColor.white
        fileprivate static let title: UIColor = UIColor.black
        fileprivate static let indicatorViewBackground: UIColor = UIColor.black
        fileprivate static let selectedTitle: UIColor = UIColor.white
    }
    
    // MARK: Error handling
    private enum IndexError: Error {
        case indexBeyondBounds(UInt)
    }
    
    // MARK: Properties
    /// The index of the selected value
    public  var index: UInt
    
    private var titles: [String]  {
        get {
            let titleLabels = selectedTitleLabelsView.subviews as! [UILabel]
            return titleLabels.map { $0.text! }
        }
        set {
            guard newValue.count > 1 else {
                return
            }
            
            sliderComponents.removeAll()
            for index in 0...(newValue.count-1){
                
                let separatorImageView = UIImageView(image: separatorImage)
                separatorImageView.contentMode = .scaleAspectFit
                let selectedTitleLabel = UILabel()
                
                selectedTitleLabel.textColor = titleColor
                selectedTitleLabel.text = newValue[index]
                selectedTitleLabel.lineBreakMode = .byTruncatingTail
                selectedTitleLabel.textAlignment = .center
                
                sliderComponents.append((separatorImageView, selectedTitleLabel))
            }
            
            imageSeparatorView.subviews.forEach({ $0.removeFromSuperview() })
            selectedTitleLabelsView.subviews.forEach({ $0.removeFromSuperview() })
            
            for (inactiveLabel, activeLabel) in sliderComponents {
                imageSeparatorView.addSubview(inactiveLabel)
                selectedTitleLabelsView.addSubview(activeLabel)
            }
            setNeedsLayout()
        }
    }
    
    /**
     Values to show on the thumb title
     */
    public var titlesValues: [String]  = [] {
        didSet{
            if showThumbLabels{
                titles = titlesValues
            }else{
                var values : [String] = []
                for _ in titlesValues{
                    values.append("")
                }
                titles = values
            }
        }
    }
    
    /// Whether the indicator should bounce when selecting a new index. Defaults to true
    public var bouncesOnChange = true
    /// Whether the the control should always send the .ValueChanged event, regardless of the index remaining unchanged after interaction. Defaults to false
    public var alwaysAnnouncesValue = false
    /// Whether to send the .ValueChanged event immediately or wait for animations to complete. Defaults to true
    public var announcesValueImmediately = true
    /// Whether the the control should ignore pan gestures. Defaults to false
    public var panningDisabled = false
    /// Called when the slider change his position, containing the index of the slider in parameter
    public var sliderDidMove : (Int)->() = { _ in }
    
    /**
     A supplimentary view to show over the thumb
     */
    public var topSupplimentaryView: UIView = UIView() {
        didSet{
            topSupplimentaryView.translatesAutoresizingMaskIntoConstraints = false
            indicatorView.addSubview(topSupplimentaryView)
            
            topSupplimentaryView.widthAnchor.constraint(equalToConstant: topSupplimentaryView.frame.width).isActive = true
            topSupplimentaryView.heightAnchor.constraint(equalToConstant: topSupplimentaryView.frame.height).isActive = true
            topSupplimentaryView.centerXAnchor.constraint(equalTo: indicatorView.centerXAnchor).isActive = true
         
            let yConstraint = self.frame.height*(-1)-5
            topSupplimentaryView.centerYAnchor.constraint(equalTo: indicatorView.centerYAnchor, constant: yConstraint).isActive = true
        }
    }
    
    /**
     A supplimentary view to show under the thumb
     */
    public var bottomSupplimentaryView: UIView = UIView() {
        didSet{
            bottomSupplimentaryView.translatesAutoresizingMaskIntoConstraints = false
            indicatorView.addSubview(bottomSupplimentaryView)
            
            bottomSupplimentaryView.widthAnchor.constraint(equalToConstant: bottomSupplimentaryView.frame.width).isActive = true
            bottomSupplimentaryView.heightAnchor.constraint(equalToConstant: bottomSupplimentaryView.frame.height).isActive = true
            bottomSupplimentaryView.centerXAnchor.constraint(equalTo: indicatorView.centerXAnchor).isActive = true
            
            let yConstraint = self.frame.height+5
            bottomSupplimentaryView.centerYAnchor.constraint(equalTo: indicatorView.centerYAnchor, constant: yConstraint).isActive = true
        }
    }
    
    /// The indicator view's border width
    public var indicatorViewBorderWidth: CGFloat {
        get {
            return indicatorView.layer.borderWidth
        }
        set {
            indicatorView.layer.borderWidth = newValue
        }
    }
    
    /// The indicator view's border width
    public var indicatorViewBorderColor: CGColor? {
        get {
            return indicatorView.layer.borderColor
        }
        set {
            indicatorView.layer.borderColor = newValue
        }
    }
    
    /// The selected title's font
    public var selectedTitleFont: UIFont = UILabel().font {
        didSet {
            indicatorView.titleLabel.font = selectedTitleFont
        }
    }
    
    // MARK: Inspectable properties
    
    /// The control's and indicator's corner radius
    @IBInspectable public var radius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            indicatorView.radius = newValue - indicatorViewInset
            separatorsArray.forEach { $0.layer.cornerRadius = indicatorView.radius }
        }
    }
    
    /// The image of the thumb
   
    
    
    /// The indicator view's background color
    @IBInspectable public var indicatorViewBackgroundColor: UIColor? {
        get {
            return indicatorView.backgroundColor
        }
        set {
            indicatorView.backgroundColor = newValue
        }
    }
    
    /// The indicator view's inset. Defaults to 2.0
    @IBInspectable public var indicatorViewInset: CGFloat = 2.0 {
        didSet {
            if indicatorViewInset<0{
                indicatorViewInset = 0
            }
            setNeedsLayout()
        }
    }

    /// The text color of the selected title
   @IBInspectable
    public var titleColor: UIColor {
        didSet {
            selectedTitleLabels.forEach {
                $0.textColor = titleColor
                indicatorView.titleLabel.textColor = titleColor
            }
        }
    }
    
    /// The number of steps
    @IBInspectable public var stepsCount: Int = 1 {
        didSet {
            var titles : [String] = []
            for i in 0..<stepsCount{
                titles.append("\(i)")
            }
            self.titlesValues = titles
        }
    }
    
    /// Whether to show the label on the thumb or not
    @IBInspectable public var showThumbLabels: Bool = false{
        didSet{
            let newValues = titlesValues
            titlesValues = newValues
        }
    }
    
    /// The image of the separators
    @IBInspectable public var separatorImage: UIImage = UIImage(){
        didSet{
           sliderComponents.removeAll()
            let newTitles = titles
            self.titles = newTitles
        }
    }
    
    // MARK: - Private properties
    fileprivate let imageSeparatorView = UIView()
    fileprivate let selectedTitleLabelsView = UIView()
    
    
    var indicatorView = IndicatorView(){
        didSet {
            
           
             self.addSubview(indicatorView)
            layoutIfNeeded()

        //finishInit()
         //   indicatorView.finishInit()
            bringSubview(toFront: indicatorView)
       
        }
    }
    
    fileprivate var initialIndicatorViewFrame: CGRect?
    
    fileprivate var tapGestureRecognizer: UITapGestureRecognizer!
    fileprivate var panGestureRecognizer: UIPanGestureRecognizer!
    
    fileprivate var width: CGFloat { return bounds.width }
    fileprivate var height: CGFloat { return bounds.height }
    fileprivate var titleLabelsCount: Int { return imageSeparatorView.subviews.count }
    fileprivate var separatorsArray: [UIImageView] { return imageSeparatorView.subviews as! [UIImageView] }
    fileprivate var selectedTitleLabels: [UILabel] { return selectedTitleLabelsView.subviews as! [UILabel] }
    fileprivate var totalInsetSize: CGFloat { return indicatorViewInset * 2.0 }
    fileprivate lazy var defaultTitles: [String] = [""]
    fileprivate var sliderComponents: [(UIImageView, UILabel)] = []
    fileprivate let trackView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
    fileprivate var trackViewWidthConstraint : NSLayoutConstraint
    
    // MARK: Initializers
    required public init?(coder aDecoder: NSCoder) {
        index = 0
        titleColor = Color.title
        titleColor = Color.selectedTitle
        trackView.translatesAutoresizingMaskIntoConstraints = false
        trackViewWidthConstraint = trackView.widthAnchor.constraint(equalToConstant: 0)
        trackViewWidthConstraint.isActive = true
        super.init(coder: aDecoder)
        titles = defaultTitles
        finishInit()
    }
    public init(frame: CGRect,
                titles: [String],
                index: UInt,
                backgroundColor: UIColor,
                titleColor: UIColor,
                indicatorViewBackgroundColor: UIColor,
                selectedTitleColor: UIColor) {
        self.index = index
        self.titleColor = titleColor
        self.titleColor = selectedTitleColor
        trackView.translatesAutoresizingMaskIntoConstraints = false
        trackViewWidthConstraint = trackView.widthAnchor.constraint(equalToConstant: 0)
        trackViewWidthConstraint.isActive = true
        super.init(frame: frame)
        self.titles = titles
        self.backgroundColor = backgroundColor
        self.indicatorViewBackgroundColor = indicatorViewBackgroundColor
        finishInit()
    }
    @available(*, deprecated, message: "Use init(frame:titles:index:backgroundColor:titleColor:indicatorViewBackgroundColor:selectedTitleColor:) instead.")
    convenience override public init(frame: CGRect) {
        self.init(frame: frame,
                  titles: [""],
                  index: 0,
                  backgroundColor: Color.background,
                  titleColor: Color.title,
                  indicatorViewBackgroundColor: Color.indicatorViewBackground,
                  selectedTitleColor: Color.selectedTitle)
    }
    
    @available(*, unavailable, message: "Use init(frame:titles:index:backgroundColor:titleColor:indicatorViewBackgroundColor:selectedTitleColor:) instead.")
    convenience init() {
        self.init(frame: CGRect.zero,
                  titles: [""],
                  index: 0,
                  backgroundColor: Color.background,
                  titleColor: Color.title,
                  indicatorViewBackgroundColor: Color.indicatorViewBackground,
                  selectedTitleColor: Color.selectedTitle)
    }
    
    fileprivate func finishInit() {
        layer.masksToBounds = false
        
        indicatorView.backgroundColor = .black
        indicatorView.layer.borderWidth = 2
        indicatorView.layer.borderColor = UIColor.white.cgColor
    
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Slider.tapped(_:)))
        addGestureRecognizer(tapGestureRecognizer)
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(Slider.panned(_:)))
        panGestureRecognizer.delegate = self
        addGestureRecognizer(panGestureRecognizer)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        guard titleLabelsCount > 1 else {
            return
        }
        
        imageSeparatorView.frame = bounds
        selectedTitleLabelsView.frame = bounds
        
        indicatorView.frame = elementFrame(forIndex: index)
        indicatorView.titleLabel.text = "\(titles[Int(index)])"
        
        for index in 0...titleLabelsCount-1 {
            let frame = elementFrame(forIndex: UInt(index))
            imageSeparatorView.subviews[index].frame = frame
            selectedTitleLabelsView.subviews[index].frame = frame
        }
        
        let thumbImageView : UIImageView = UIImageView(image: indicatorView.thumbImage)
        thumbImageView.translatesAutoresizingMaskIntoConstraints = false
        
        thumbImageView.contentMode = .scaleAspectFit
      
        
        addSubview(trackView)
        trackView.backgroundColor = UIColor.clear
        trackView.translatesAutoresizingMaskIntoConstraints = false
        trackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        trackView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        trackView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        let newTrackViewWidth : CGFloat = width/CGFloat(titleLabelsCount)*CGFloat(index)
        trackViewWidthConstraint.constant = newTrackViewWidth
        if test{
        addSubview(imageSeparatorView)
        }
      
        addSubview(indicatorView)
      indicatorView.addSubview(thumbImageView)  
        
        thumbImageView.trailingAnchor.constraint(equalTo: indicatorView.trailingAnchor).isActive = true
        thumbImageView.topAnchor.constraint(equalTo: indicatorView.topAnchor).isActive = true
        thumbImageView.bottomAnchor.constraint(equalTo: indicatorView.bottomAnchor).isActive = true
        thumbImageView.leadingAnchor.constraint(equalTo: indicatorView.leadingAnchor).isActive = true
        indicatorView.bringSubview(toFront: indicatorView.titleLabel)

    }
    
    // MARK: Index Setting
    /*
     Sets the control's index.
     
     - parameter index:    The new index
     - parameter animated: (Optional) Whether the change should be animated or not. Defaults to true.
     
     - throws: An error of type IndexBeyondBounds(UInt) is thrown if an index beyond the available indices is passed.
     */
    public func setIndex(_ index: UInt, animated: Bool = true) throws {
        guard separatorsArray.indices.contains(Int(index)) else {
            throw IndexError.indexBeyondBounds(index)
        }
        let oldIndex = self.index
        self.index = index
        moveIndicatorViewToIndex(animated, shouldSendEvent: (self.index != oldIndex || alwaysAnnouncesValue))
    }
    
    // MARK: Animations
    fileprivate func moveIndicatorViewToIndex(_ animated: Bool, shouldSendEvent: Bool) {
        if animated {
            if shouldSendEvent && announcesValueImmediately {
                sendActions(for: .valueChanged)
            }
            UIView.animate(withDuration: bouncesOnChange ? Animation.withBounceDuration : Animation.withoutBounceDuration,
                           delay: 0.0,
                           usingSpringWithDamping: bouncesOnChange ? Animation.springDamping : 1.0,
                           initialSpringVelocity: 0.0,
                           options: [UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions.curveEaseOut],
                           animations: {
                            () -> Void in
                            self.moveIndicatorView()
            }, completion: { (finished) -> Void in
                if finished && shouldSendEvent && !self.announcesValueImmediately {
                    self.sendActions(for: .valueChanged)
                }
            })
        } else {
            moveIndicatorView()
            sendActions(for: .valueChanged)
        }
    }
    
    // MARK: Helpers
    fileprivate func elementFrame(forIndex index: UInt) -> CGRect {
        let elementWidth = (width-30 - totalInsetSize) / CGFloat(titleLabelsCount-1)
        return CGRect(x: CGFloat(index) * elementWidth,
                      y: indicatorViewInset,
                      width: height - totalInsetSize,
                      height: height - totalInsetSize)
    }
    fileprivate func nearestIndex(toPoint point: CGPoint) -> UInt {
        let distances = separatorsArray.map { abs(point.x - $0.center.x) }
        return UInt(distances.index(of: distances.min()!)!)
    }
    fileprivate func moveIndicatorView() {
        indicatorView.frame = separatorsArray[Int(self.index)].frame
        
        layoutIfNeeded()
    }
    
    // MARK: Action handlers
    @objc fileprivate func tapped(_ gestureRecognizer: UITapGestureRecognizer!) {
        /*let location = gestureRecognizer.location(in: self)
        try! setIndex(nearestIndex(toPoint: location))
        indicatorView.titleLabel.text = "\(titles[Int(index)])"
        
        sliderDidMove(Int(index))
        let newTrackViewWidth : CGFloat = width/CGFloat(titleLabelsCount)*CGFloat(index)
        trackView.translatesAutoresizingMaskIntoConstraints = false
        trackView.widthAnchor.constraint(equalToConstant: newTrackViewWidth).isActive = true*/
    }
    
    @objc fileprivate func panned(_ gestureRecognizer: UIPanGestureRecognizer!) {
       /* guard !panningDisabled else {
            return
        }
        
        switch gestureRecognizer.state {
        case .began:
            initialIndicatorViewFrame = indicatorView.frame
        case .changed:
            var frame = initialIndicatorViewFrame!
            frame.origin.x += gestureRecognizer.translation(in: self).x
            frame.origin.x = max(min(frame.origin.x, bounds.width - indicatorViewInset - frame.width), indicatorViewInset)
            indicatorView.frame = frame
            indicatorView.titleLabel.text = "\(titles[Int(nearestIndex(toPoint: indicatorView.center))])"
            
            sliderDidMove(Int(nearestIndex(toPoint: indicatorView.center)))
        case .ended, .failed, .cancelled:
            try! setIndex(nearestIndex(toPoint: indicatorView.center))
        default: break
        }*/
    }
    func setbottomLabelText(message:String){
        
        let bottomCustomSupplimentaryView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        
       
        let bottomLabel = UILabel(frame: CGRect(x: 0, y: 0, width: bottomCustomSupplimentaryView.frame.width, height: bottomCustomSupplimentaryView.frame.height-5))
        bottomLabel.textColor = UIColor.lightGray
        bottomLabel.textAlignment = .center
        bottomLabel.text = message
        bottomLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
       
        bottomCustomSupplimentaryView.addSubview(bottomLabel)
        self.bottomSupplimentaryView = bottomCustomSupplimentaryView
    }
    
    func setTabOfPointeur(pointeurTab:[Pointeur],setepsCount:Int,view:UIView,color:UIColor) {
       
    sliderComponents.removeAll()
    self.stepsCount = setepsCount
       self.test = true
        self.index = UInt((pointeurTab.last?.index)!)
       self.separatorImage = #imageLiteral(resourceName: "separator")
       self.indicatorView = IndicatorView()
        
        view.backgroundColor = color
       // view.alpha = 0.1
        view.layer.opacity = 0.1
        var max = 0
        for p in pointeurTab {
            if p.index! > max {
            max = p.index!
            }
        
        }
        
        
         view.widthAnchor.constraint(equalToConstant: CGFloat(self.frame.maxX)/CGFloat(stepsCount)*(CGFloat(max+3))).isActive = true
//     self.addSubview(view)
//       self.bringSubview(toFront: view)
    for p in pointeurTab {
            self.test = false
            let indicView = IndicatorView()
                indicView.thumbImage = p.poiteurImage!
            self.index = UInt(p.index!)
            self.indicatorView = indicView
            self.setbottomLabelText(message:p.message!)
                  }
        
    }
    
}

// MARK: - UIGestureRecognizerDelegate
extension Slider: UIGestureRecognizerDelegate {
    override open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == panGestureRecognizer {
            return indicatorView.frame.contains(gestureRecognizer.location(in: self))
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
}



/**
 This class represent the thumb of the slider
 */

public class IndicatorView: UIView {
    // MARK: Properties
    var titleLabel: UILabel = UILabel()
    
    fileprivate var radius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = radius
        }
    }
    
    @IBInspectable public var thumbImage: UIImage = UIImage(){
        didSet {
            self.layer.borderWidth = 0
            self.backgroundColor = .clear
            let size = CGSize(width: self.frame.height, height: self.frame.height)
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
            thumbImage.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: size.width, height: size.height)))
            UIGraphicsEndImageContext()
            
        }
    }
    


    
    // MARK: Lifecycle
    init() {
        
        super.init(frame: CGRect.zero)
        finishInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        finishInit()
    }
    
    fileprivate func finishInit() {
        layer.masksToBounds = false
        radius = frame.height/2
        
        titleLabel.frame = frame
        titleLabel.contentMode = .center
        titleLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 10.0)!
        addSubview(titleLabel)
        bringSubview(toFront: titleLabel)
    }
}
