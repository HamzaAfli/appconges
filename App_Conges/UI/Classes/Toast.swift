//
//  Toast.swift
//  App_Conges
//
//  Created by Hamza on 8/2/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
import UIKit
class Toast
{
    class private func showAlert(backgroundColor:UIColor, textColor:UIColor, message:String,textField:UITextField,iconName:String)->UILabel
    {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let label = UILabel(frame: CGRect.zero)
        label.textAlignment = NSTextAlignment.center
        label.text = message
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        label.adjustsFontSizeToFitWidth = true
        
        label.backgroundColor =  backgroundColor
        label.textColor = textColor
        
        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 6, height: 3)
        label.layer.shadowOpacity = 0.5
        
       let x = textField.superview?.convert(textField.frame, to: nil).origin.x
         let y = textField.superview?.convert(textField.frame, to: nil).origin.y
        label.frame = CGRect(x: x!, y: y!, width:textField.frame.width-(textField.rightView?.frame.width)!, height: 30)
        label.layer.masksToBounds = true
        label.layer.cornerRadius = label.frame.height / 3
        label.alpha = 1
        label.layer.opacity = 1
        
        /*bounds==206.5
         bounds==34.0
         bounds==122.5
         -------------
         bounds==153.5
         bounds==28.5
         bounds==90.5
         -------------
         bounds==(186.5, 33.0)
         bounds==(61.5, 33.0)
         bounds==(123.5, 33.0)
*/
        
        appDelegate.window!.addSubview(label)
        
        var basketTopFrame: CGRect = label.frame;
       
        basketTopFrame.origin.x = textField.frame.maxX-textField.frame.width//-(textField.rightView?.frame.width)!
       
         textField.rightView = UIImageView(image: UIImage(named: "warning-sign"))
        UIView.animate(withDuration:2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                label.frame = basketTopFrame
            
        },  completion: {
            (value: Bool) in
            UIView.animate(withDuration:2.0, delay: 2.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                label.alpha = 0
               

            },  completion: {
                (value: Bool) in
                textField.rightView = UIImageView(image: UIImage(named: iconName))
                label.removeFromSuperview()
                

            })
            
        })
        return label
    }
    
    class func showPositiveMessage(message:String,textField:UITextField,iconName:String)
    {
        showAlert(backgroundColor: UIColor.green, textColor: UIColor.white, message: message,textField: textField,iconName: iconName).removeFromSuperview()
    }
    class func showNegativeMessage(message:String,textField:UITextField,iconName:String)->UILabel
    {
      return  showAlert(backgroundColor: UIColor(colorLiteralRed: 132/255, green: 116/255, blue: 154/255, alpha: 0.5), textColor: UIColor.white, message: message,textField:textField,iconName: iconName)
    }
}
