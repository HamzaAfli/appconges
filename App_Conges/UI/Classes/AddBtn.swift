//
//  AddBtn.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/20/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class AddBtn: UIButton {

    override func awakeFromNib() {
    self.layer.cornerRadius = self.frame.width/2
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.2, height: 6)
        self.layer.shadowOpacity = 1

    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
