//
//  File.swift
//  App_Conges
//
//  Created by Hamza on 8/13/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
import UIKit
class Autorisation{
    var img:UIImage!
    var name:String!
    var date : String!
    var etat : String!
    var periode: String!
    var dure:String!
    var recuperation:String!
    var id:Int!

    init( img:UIImage!,name:String!,date : String!,etat : String!, periode: String!, dure:String!,recuperation:String!,id:Int) {
        self.img = img
        self.name = name
        self.date = date
        self.etat = etat
        self.periode = periode
        self.dure = dure
        self.recuperation = recuperation
        self.id = id
        
    }
    init() {
        
    }

    
}
