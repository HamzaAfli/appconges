//
//  RoundView.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/20/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class RoundView: UIView {
    @IBInspectable open var color : UIColor = UIColor.white
    override func awakeFromNib() {
           }
    
    func makeProgressLine(value:String){
       
        if  let val = Float(value){
           
            let val2 = val * 360 / 100
         
        // set up some values to use in the curve
        let ovalStartAngle = CGFloat(0 * Double.pi/180)
        let ovalEndAngle = CGFloat((0.0001+Double(val2)) * Double.pi/180)
        let ovalRect =  CGRect(x: 0, y: 0, width:self.frame.width, height:self.frame.height)
            //CGRect(x:97.5, y:58.5, width:125, 125)
       
        // create the bezier path
        let ovalPath = UIBezierPath()
      
        
        ovalPath.addArc(withCenter:CGPoint(x: ovalRect.midX, y: ovalRect.midY),radius: ovalRect.width / 2,startAngle: ovalStartAngle,endAngle: ovalEndAngle, clockwise: true)
        
        // create an object that represents how the curve
        // should be presented on the screen
        let progressLine = CAShapeLayer()
        progressLine.path = ovalPath.cgPath
        progressLine.strokeColor = color.cgColor
        progressLine.fillColor = UIColor.clear.cgColor
        progressLine.lineWidth = 2
        progressLine.lineCap = kCALineCapRound
        
        // add the curve to the screen
        self.layer.addSublayer(progressLine)
        
        // create a basic animation that animates the value 'strokeEnd'
        // from 0.0 to 1.0 over 3.0 seconds
        let animateStrokeEnd = CABasicAnimation(keyPath: "strokeEnd")
        animateStrokeEnd.duration = 2.0
        animateStrokeEnd.fromValue = 0.0
        animateStrokeEnd.toValue = 1.0
        
        // add the animation
        progressLine.add(animateStrokeEnd, forKey: "animate stroke end animation")
        
        
        }else {
            self.layer.cornerRadius = self.frame.width/2
            self.layer.borderWidth = 2
            self.layer.borderColor = color.cgColor
        
        }
   
   

}
}
