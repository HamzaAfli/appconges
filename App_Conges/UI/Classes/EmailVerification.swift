//
//  EmailVerification.swift
//  App_Conges
//
//  Created by Hamza on 7/29/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
class  EmailVerification {
    
    static func verfyEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
}
