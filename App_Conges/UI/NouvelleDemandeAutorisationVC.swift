//
//  NouvelleDemandeAutorisationVC.swift
//  App_Conges
//
//  Created by Hamza on 8/17/17.
//  Copyright © 2017 Hamza. All rights reserved.
//



    import UIKit
    import SCLAlertView
    
    class NouvelleDemandeAutorisationVC: ViewController {
        //MARK:- Outlet
        
        @IBOutlet weak var motifLBL: UILabel!
        @IBOutlet weak var dateDeRecuperationLBL: UILabel!
        @IBOutlet weak var jusquAuLBL: UILabel!
        @IBOutlet weak var apartirDeLBL: UILabel!
        @IBOutlet weak var dateDemandeLBL: UILabel!
        @IBOutlet weak var dateDemandeTF: UITextField!
        @IBOutlet weak var motifTF: UITextField!
        @IBOutlet weak var dateRecuperationTF: UITextField!
        @IBOutlet weak var jusquAuTF: UITextField!
        @IBOutlet weak var apartirDeTF: UITextField!
        //MARK:- Variavle
        var dateDeb:Date?
        var dateFin:Date?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            changeColorOfOneCaractarFromLabel(label: dateDemandeLBL, caractarPosition: (dateDemandeLBL.text?.length)!-1)
            changeColorOfOneCaractarFromLabel(label: dateDeRecuperationLBL, caractarPosition: (dateRecuperationTF.text?.length)!-1)
            changeColorOfOneCaractarFromLabel(label: jusquAuLBL, caractarPosition: (jusquAuLBL.text?.length)!-1)
            changeColorOfOneCaractarFromLabel(label: apartirDeLBL, caractarPosition: (apartirDeLBL.text?.length)!-1)
            
            dateDemandeTF.rightViewMode = .always
            dateDemandeTF.rightView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
            dateRecuperationTF.rightViewMode = .always
            dateRecuperationTF.rightView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
            jusquAuTF.rightViewMode = .always
            jusquAuTF.rightView = UIImageView(image:#imageLiteral(resourceName: "arrow-point-to-down-Black"))
            apartirDeTF.rightViewMode = .always
            apartirDeTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            
            // Do any additional setup after loading the view.
        }
        
        
        
        override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
            super.viewWillTransition(to: size, with: coordinator)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.setNavigationBarItem()
            
            
        }
        
        //Mark:-Action
        
        
        
        @IBAction func dateDemandeTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.date
            
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = 100
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            
            
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChangedFordateDemandeTF), for: UIControlEvents.valueChanged)
        }
        
        @IBAction func dateRecuperationTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.date
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = 100
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChangedFordateRecuperationTF), for: UIControlEvents.valueChanged)
        }
        
        
        @IBAction func dateRecuperationEditingdidEnd(_ sender: UITextField) {
            if dateDemandeTF.text != "" && dateRecuperationTF.text != ""{
                
                if compareDate(date1: dateDeb!, date2: dateFin!) {
                    SCLAlertView().showTitle(
                        "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    dateDemandeTF.text = ""
                    dateRecuperationTF.text = ""
                }
            }
        }
        @IBAction func dateDemandeEditingDidEnd(_ sender: UITextField) {
            if dateRecuperationTF.text != "" && dateDemandeTF.text != "" {
                if compareDate(date1: dateDeb!, date2: dateFin!) {
                    SCLAlertView().showTitle(
                        "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    dateDemandeTF.text = ""
                    dateRecuperationTF.text = ""
                }
            }
            
        }
        
        //Mark:-Function
        
        
        @objc private func datePickerValueChangedFordateDemandeTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            
            dateDeb = sender.date
            
            dateFormatter.dateFormat = "dd/MMM/yyyy"
            dateDemandeTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        @objc private func datePickerValueChangedFordateRecuperationTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFin = sender.date
            
            dateFormatter.dateFormat = "dd/MMM/yyyy"
            dateRecuperationTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        
        
    }
    //Mark:-Extension
    extension NouvelleDemandeAutorisationVC : SlideMenuControllerDelegate {
        
        func leftWillOpen() {
            print("SlideMenuControllerDelegate: leftWillOpen")
        }
        
        func leftDidOpen() {
            print("SlideMenuControllerDelegate: leftDidOpen")
        }
        
        func leftWillClose() {
            print("SlideMenuControllerDelegate: leftWillClose")
        }
        
        func leftDidClose() {
            print("SlideMenuControllerDelegate: leftDidClose")
        }
        
        func rightWillOpen() {
            print("SlideMenuControllerDelegate: rightWillOpen")
        }
        
        func rightDidOpen() {
            print("SlideMenuControllerDelegate: rightDidOpen")
        }
        
        func rightWillClose() {
            print("SlideMenuControllerDelegate: rightWillClose")
        }
        
        func rightDidClose() {
            print("SlideMenuControllerDelegate: rightDidClose")
        }
        
        
        
}
