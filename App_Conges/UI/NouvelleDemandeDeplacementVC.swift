//
//  NouvelleDemandeDeplacementVC.swift
//  App_Conges
//
//  Created by Hamza on 8/17/17.
//  Copyright © 2017 Hamza. All rights reserved.
//



    
    import UIKit
    import  SCLAlertView
    
    class NouvelleDemandeDeplacementVC: ViewController {
        
        //MARK:- Outlet
        
        
        @IBOutlet weak var motifLBL: UILabel!
       
        @IBOutlet weak var AvecLBL: UILabel!
        @IBOutlet weak var dateDebLBL: UILabel!
        @IBOutlet weak var heurDebLBL: UILabel!
        @IBOutlet weak var dateFinLBL: UILabel!
        @IBOutlet weak var heurDeFinLBL: UILabel!
        @IBOutlet weak var typeLBL: UILabel!
        @IBOutlet weak var ClientLBL: UILabel!
        @IBOutlet weak var projetLBL: UILabel!
        @IBOutlet weak var adresseDestLBL: UILabel!
        @IBOutlet weak var paysLBL: UILabel!
        @IBOutlet weak var regionDestLBL: UILabel!
        @IBOutlet weak var lieuDeLogementLBL: UILabel!
        
        
        
        @IBOutlet weak var avecTF: UITextField!
        @IBOutlet weak var heureDebTF: UITextField!
        
        @IBOutlet weak var dateDebTF: UITextField!
        
        @IBOutlet weak var dateFinTF: UITextField!
        
        @IBOutlet weak var heureFinTF: UITextField!
        
        @IBOutlet weak var clientTF: UITextField!
        
        @IBOutlet weak var typeTf: UITextField!
        
        @IBOutlet weak var projetTF: UITextField!
        
        @IBOutlet weak var adresseDestTf: UITextField!
        
        @IBOutlet weak var paysTF: UITextField!
        
        @IBOutlet weak var motifTF: UITextField!
        @IBOutlet weak var lieuDeLogementTF: UITextField!
        @IBOutlet weak var regionDestTF: UITextField!
        
        //MARK:- Variables
        
        var dateDeb : Date?
        var dateFin :Date?
        var heureDeb : Date?
        var heureFin :Date?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            changeColorOfOneCaractarFromLabel(label: AvecLBL, caractarPosition: (AvecLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: dateDebLBL, caractarPosition:  (dateDebLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: heurDebLBL, caractarPosition:  (heurDebLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: heurDeFinLBL, caractarPosition:  (heurDeFinLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: dateFinLBL, caractarPosition:  (dateFinLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: typeLBL, caractarPosition:  (typeLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: ClientLBL, caractarPosition:  (ClientLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: projetLBL, caractarPosition:  (projetLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: adresseDestLBL, caractarPosition:  (adresseDestLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: paysLBL, caractarPosition:  (paysLBL.text?.length)!-1, textSize: 16)
            changeColorOfOneCaractarFromLabel(label: regionDestLBL, caractarPosition:  (regionDestLBL.text?.length)!-1, textSize: 15)
            changeColorOfOneCaractarFromLabel(label: lieuDeLogementLBL, caractarPosition:  (lieuDeLogementLBL.text?.length)!-1, textSize: 15)
            
            // Do any additional setup after loading the view.
            
            heureDebTF.rightViewMode = .always
            heureDebTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            heureFinTF.rightViewMode = .always
            heureFinTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            dateFinTF.rightViewMode = .always
            dateFinTF.rightView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
            dateDebTF.rightViewMode = .always
            dateDebTF.rightView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
            typeTf.rightViewMode = .always
            typeTf.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            clientTF.rightViewMode = .always
            clientTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            projetTF.rightViewMode = .always
            projetTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            paysTF.rightViewMode = .always
            paysTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            regionDestTF.rightViewMode = .always
            regionDestTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            lieuDeLogementTF.rightViewMode = .always
            lieuDeLogementTF.rightView = UIImageView(image: #imageLiteral(resourceName: "arrow-point-to-down-Black"))
            
        }
        
        
        override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
            super.viewWillTransition(to: size, with: coordinator)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.setNavigationBarItem()
            
            
        }
        
        
        //MARK:- Action
        @IBAction func dateDebTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.date
            
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = 100
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            
            
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChangedForDateDebTF), for: UIControlEvents.valueChanged)
        }
        
        @IBAction func dateFinTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.date
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            components.year = 100
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePickerView.minimumDate = minDate
            datePickerView.maximumDate = maxDate
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChangedForDateFinTF), for: UIControlEvents.valueChanged)
        }
        
        
        @IBAction func dateFinEditingdidEnd(_ sender: UITextField) {
            if dateDebTF.text != "" && dateFinTF.text != ""{
                
                if compareDate(date1: dateDeb!, date2: dateFin!) {
                    SCLAlertView().showTitle(
                        "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    dateDebTF.text = ""
                    dateFinTF.text = ""
                }
            }
        }
        @IBAction func datedebEditingDidEnd(_ sender: UITextField) {
            if dateFinTF.text != "" && dateDebTF.text != "" {
                if compareDate(date1: dateDeb!, date2: dateFin!) {
                    SCLAlertView().showTitle(
                        "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                    dateDebTF.text = ""
                    dateFinTF.text = ""
                }
            }
            
        }
        
        @IBAction func heureDebTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.time
            
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            components.hour = 0
            components.minute = 0
            components.second = 0
            //        let minDate = Calendar.current.date(byAdding: components, to: Date())
            //
            //        components.year = 100
            //        let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            //  datePickerView.minimumDate = minDate
            //  datePickerView.maximumDate = maxDate
            
            
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(heurePickerValueChangedForDateDebTF), for: UIControlEvents.valueChanged)
        }
        
        @IBAction func heureFinTFEditing(_ sender: Any) {
            let datePickerView:UIDatePicker = UIDatePicker()
            
            datePickerView.datePickerMode = UIDatePickerMode.time
            var components = DateComponents()
            components.year = 0
            components.month = 0
            components.day = 0
            components.hour = 0
            components.minute = 0
            components.second = 0
            //        let minDate = Calendar.current.date(byAdding: components, to: Date())
            //
            //        components.year = 100
            //        let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            // datePickerView.minimumDate = minDate
            //datePickerView.maximumDate = maxDate
            (sender as! UITextField).inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(heurePickerValueChangedForDateFinTF), for: UIControlEvents.valueChanged)
        }
        
        
        @IBAction func heureFinEditingdidEnd(_ sender: UITextField) {
            //        if heureDebTF.text != "" && heureFinTF.text != ""{
            //
            //            if  heureDeb! > heureFin! {
            //                SCLAlertView().showTitle(
            //                    "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
            //                heureDebTF.text = ""
            //                heureFinTF.text = ""
            //            }
            //        }
        }
        @IBAction func heuredebEditingDidEnd(_ sender: UITextField) {
            //
            //        if heureFinTF.text != "" && heureDebTF.text != "" {
            //
            //            if heureDeb! > heureFin! {
            //                SCLAlertView().showTitle(
            //                    "",subTitle: "saisie une date de début et date de fin valide",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
            //                heureDebTF.text = ""
            //                heureFinTF.text = ""
            //            }
            //        }
            
        }
        
        //MARK:- Function
        
        private func changeColorOfOneCaractarFromLabel(label:UILabel,caractarPosition:Int,textSize:Int){
            
            let   mutableString = NSMutableAttributedString(string: label.text!, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: CGFloat(textSize))!])
            mutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location:label.text!.length-1,length:1))
            // set label Attribute
            label.attributedText = mutableString
        }
        
        
        @objc private func datePickerValueChangedForDateDebTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            
            dateDeb = sender.date
            
            dateFormatter.dateFormat = "dd/MMM/yyyy"
            dateDebTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        @objc private func datePickerValueChangedForDateFinTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFin = sender.date
            
            dateFormatter.dateFormat = "dd/MMM/yyyy"
            dateFinTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        @objc private func heurePickerValueChangedForDateDebTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.none
            
            dateFormatter.timeStyle = DateFormatter.Style.medium
            
            heureDeb = sender.date
            
            heureDebTF.text = dateFormatter.string(from: sender.date)
            
        }
        
        @objc private func heurePickerValueChangedForDateFinTF(sender:UIDatePicker) {
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.none
            
            dateFormatter.timeStyle = DateFormatter.Style.medium
            heureFin = sender.date
            
            heureFinTF.text = dateFormatter.string(from: sender.date)
            
        }
    }
    
    
    
    //Mark:-SlideMenu
    extension NouvelleDemandeDeplacementVC : SlideMenuControllerDelegate {
        
        func leftWillOpen() {
            print("SlideMenuControllerDelegate: leftWillOpen")
        }
        
        func leftDidOpen() {
            print("SlideMenuControllerDelegate: leftDidOpen")
        }
        
        func leftWillClose() {
            print("SlideMenuControllerDelegate: leftWillClose")
        }
        
        func leftDidClose() {
            print("SlideMenuControllerDelegate: leftDidClose")
        }
        
        func rightWillOpen() {
            print("SlideMenuControllerDelegate: rightWillOpen")
        }
        
        func rightDidOpen() {
            print("SlideMenuControllerDelegate: rightDidOpen")
        }
        
        func rightWillClose() {
            print("SlideMenuControllerDelegate: rightWillClose")
        }
        
        func rightDidClose() {
            print("SlideMenuControllerDelegate: rightDidClose")
        }
        
        
        
}
