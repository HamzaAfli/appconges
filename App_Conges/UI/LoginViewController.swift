
//
//  LoginViewController.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/17/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import Alamofire
import BALoadingView
import SCLAlertView
class LoginViewController: ViewController {
    
    //MARK:  Outlet
    @IBOutlet weak var usernameTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var loginBtn: LoginBtn!
    @IBOutlet weak var loginLBL: UILabel!
    
    @IBOutlet weak var loadingView: BALoadingView!

    @IBOutlet weak var usernameUnderView: UIView!
    @IBOutlet weak var welcomeLBL: UILabel!
    @IBOutlet weak var sogonImg: UIImageView!
    @IBOutlet weak var passwordUnderView: UIView!
    override func viewDidLoad() {
        API.loginViewControllerReference = self
        
        
        loginLBL.popOut()
        sogonImg.popOut()
        welcomeLBL.popOut()
        usernameTf.popOut()
        usernameUnderView.popOut()
        passwordTf.popOut()
        passwordUnderView.popOut()
        loginBtn.popOut()
        
        loadingView.alpha = 0
        super.viewDidLoad()
      usernameTf.modifyClearButtonWithImage(image: UIImage(named: "clearTF")!)
       passwordTf.modifyClearButtonWithImage(image: UIImage(named: "clearTF")!)
//    
//        usernameTf.rightViewMode = .always
//        usernameTf.rightView = UIImageView(image: UIImage(named: "arrow-point-to-down"))
        usernameTf.attributedPlaceholder =
            NSAttributedString(string: "Username", attributes: [NSForegroundColorAttributeName : UIColor(colorLiteralRed: 132/255, green: 116/255, blue: 143/255, alpha: 0.8)])
        passwordTf.attributedPlaceholder =
            NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName :UIColor(colorLiteralRed: 132/255, green: 116/255, blue: 143/255, alpha: 0.8)])
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loginLBL.popIn(delay:0.1)
        sogonImg.popIn(delay:0.2)
        welcomeLBL.popIn(delay:0.3)
        usernameTf.popIn(delay:0.4)
        usernameUnderView.popIn(delay:0.5)
        passwordTf.popIn(delay:0.6)
        passwordUnderView.popIn(delay:0.7)
        loginBtn.popIn(delay: 0.8)
        loginBtn.awakeFromNib()

    }
    override func viewDidLayoutSubviews() {
        loginBtn.awakeFromNib()
    }
    
    //MARK:Action
    @IBAction func loginPressed(_ sender: Any) {
//        if EmailVerification.verfyEmail(candidate: usernameTf.text!) == false {
//         SCLAlertView().showInfo("", subTitle: "Vérifier votre adresse mail")
//        }else{
        if passwordTf.text == "" || usernameTf.text == "" {
            SCLAlertView().showInfo("", subTitle: "Nom d'utilisateur et mot de passe requis")
        }else{
        loginBtn.alpha = 0
        loadingView.alpha = 1
        loadingView.initialize()
        loadingView.segmentColor = UIColor(colorLiteralRed: 153/255, green: 179/255, blue: 54/255, alpha: 1)
        loadingView.start(.fullCircle)
            let user = "admin"
            let password = "Pr0xym-1T$$!!"
            let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
            let base64Credentials = credentialData.base64EncodedString(options: [])
            let headers = [
                "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
           //user: //https://api.myjson.com/bins/b0zsh
            //admi://https://api.myjson.com/bins/au8t9
            //"http://\(API.BASE_URL)/\(API.SERVICE)/j_spring_security_check"
            //"http://preprod-tracktime.proxym-it.tn/j_spring_security_check"
            Alamofire.request("http://preprod-tracktime.proxym-it.tn/j_spring_security_check", method: .post, parameters: ["j_username":self.usernameTf.text!,"j_password":self.passwordTf.text!,"ajax":"true"],  headers:headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
       
           // Alamofire.request("https://api.myjson.com/bins/au8t9", method: .get, parameters: ["j_username":self.usernameTf.text!,"j_password":self.passwordTf.text!,"ajax":"true"],  headers:[:]).responseJSON { response in
           
            guard response.result.isSuccess else {
                SCLAlertView().showWarning("", subTitle: "Network Connection Failed!! ")
                self.loadingView.stopAnimation()
                self.loadingView.alpha = 0
                self.loginBtn.alpha = 1
                return
            }
    
                
            let json = response.result.value as! [String:AnyObject]
            let data = Json4Swift_Base(dictionary: json as NSDictionary)
            
            if data?.success  == true {
            
            
            if  ((response.response?.allHeaderFields as? [String: String]) != nil)
                            {
              
                for cookie in (HTTPCookieStorage.shared.cookies)!{
                    if cookie.name == "JSESSIONID" {
                        API.jsessionId = cookie.value
                    }
                }
                    self.createMenuView(data: data!)
                self.loadingView.stopAnimation()
            }
           }else {
                
                SCLAlertView().showTitle(
                    "",subTitle: "Désolé, nous n'avons pas pu trouver d'utilisateur avec ce login et ce mot de passe.",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
               Timer.scheduledTimer(withTimeInterval: 6, repeats: false, block: { (t) in
                 //self.createMenuView()
               })

                //showError("", subTitle: "Désolé, nous n'avons pas pu trouver d'utilisateur avec ce login et ce mot de passe.")
                self.loadingView.stopAnimation()
                self.loadingView.alpha = 0
                self.loginBtn.alpha = 1
                self.usernameTf.text = ""
                self.passwordTf.text = ""
            
            }
        }
        }
        
        }
    
    
    deinit {
    }
    
    @IBAction func usernameEditingChanged(_ sender: UITextField) {
        if sender.text != nil && EmailVerification.verfyEmail(candidate: sender.text!){
        sender.textColor = UIColor.white
        }else{
        sender.textColor = UIColor(red: 284/255, green: 15/255, blue: 106/255, alpha: 1)
        }
        
    }
    
    
    //MARK:Private fuction
    private   func createMenuView(data:Json4Swift_Base) {
         API.data = data
        API.data?.employee?.isAdmin = true
        if ((API.data?.employee?.isAdmin)!) == false {
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let slideViewController = storyboard.instantiateViewController(withIdentifier:"SlideVC") as! SlideVC
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
     
        
        let nvc: UINavigationController = UINavigationController(rootViewController: dashboardViewController)

       
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = true
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barStyle = UIBarStyle.black
    
       // UINavigationBar.appearance().tintColor = UIColor.blue
        
        slideViewController.dashBoard = nvc
        let window = UIApplication.shared.keyWindow
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: slideViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = dashboardViewController
      //  window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        window?.rootViewController = slideMenuController
        window?.makeKeyAndVisible()
       }else {

    
        // create viewController code...
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        let slideViewController = storyboard.instantiateViewController(withIdentifier:"AdminSlideVC") as! AdminSlideVC
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier:"DashboardAdminViewController") as! DashboardAdminViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: dashboardViewController)
        
        
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = true
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barStyle = UIBarStyle.black
        
        // UINavigationBar.appearance().tintColor = UIColor.blue
        
        slideViewController.dashBoard = nvc
        let window = UIApplication.shared.keyWindow
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: slideViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = dashboardViewController
        //  window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        window?.rootViewController = slideMenuController
        window?.makeKeyAndVisible()
        
    }
    
    
    }
    
    
   
    
}


