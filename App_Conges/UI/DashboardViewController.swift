//
//  DashboardViewController.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/18/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class DashboardViewController: ViewController {
    
    //MARK: Outlet
    
    
    @IBOutlet weak var profileImg: RoundImage!
    
    @IBOutlet weak var heurDeTravialLbl: UILabel!
    
    @IBOutlet weak var noteMoyLbl: UILabel!

    @IBOutlet weak var nameLbl: UILabel!
    
    
    @IBOutlet weak var roleLbl: UILabel!
    
    @IBOutlet weak var joursTravailésLbl: UILabel!
    
    
    @IBOutlet weak var heurestravailésLbl: UILabel!
    
    
    @IBOutlet weak var retardsLbl: UILabel!
    
    @IBOutlet weak var acceptedView: RoundView!
    
    @IBOutlet weak var addBtn: AddBtn!
    
    @IBOutlet weak var nbrCongesLbl: UILabel!
    
    @IBOutlet weak var annulerView: RoundView!
    
    
    @IBOutlet weak var refuseView: RoundView!
    
    @IBOutlet weak var accepteLbl: UILabel!
    
    @IBOutlet weak var annuleLbl: UILabel!
    
    @IBOutlet weak var refuseLbl: UILabel!
    
    //MARK: Variable
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         addBtn.awakeFromNib()
        
        accepteLbl.text = "19"
        annuleLbl.text = "50"
        refuseLbl.text = "100"
          fillDashboardwithData()
        acceptedView.makeProgressLine(value: accepteLbl.text!)
        refuseView.makeProgressLine(value: refuseLbl.text!)
        annulerView.makeProgressLine(value: annuleLbl.text!)
      
        // Do any additional setup after loading the view.
    }

    deinit {
    }
    override func viewDidLayoutSubviews() {
        profileImg.awakeFromNib()
        addBtn.awakeFromNib()
        acceptedView.awakeFromNib()
        refuseView.awakeFromNib()
        annulerView.awakeFromNib()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "DASHBOARD"
       
    }
    
    //Mark:-Action
    @IBAction func AddPressed(_ sender: Any) {
      
        let nouvelleDemandeVC = storyboard?.instantiateViewController(withIdentifier: "NouvelleDemandeVC") as! NouvelleDemandeVC
        SlideVC.s_isDashBoardVC = false
        SlideVC.s_isNouvelleDemandeVC = true
        let nvc: UINavigationController = UINavigationController(rootViewController: nouvelleDemandeVC)
        
        
        nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = true
        nvc.navigationBar.tintColor = UIColor.white
        nvc.navigationBar.barStyle = UIBarStyle.black
        self.slideMenuController()?.changeMainViewController(nvc, close: true)

    }
    
    //Mark:-Private Function
    private func fillDashboardwithData(){
        
        
        nameLbl.text = API.data?.username!
        heurDeTravialLbl.text = String(describing: API.data!.employee!.hmTravail!)
        noteMoyLbl.text = String(describing: API.data!.employee!.noteMoyenne!)
        heurestravailésLbl.text = String(describing: API.data!.employee!.heureTravaille!)
        joursTravailésLbl.text = String(describing: API.data!.employee!.joursTravaille!)
        retardsLbl.text = String(describing: API.data!.employee!.retard!)
        nbrCongesLbl.text = String(describing: API.data!.employee!.solde!)
        
         }
    
       
}


//Mark:-Extension
extension DashboardViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
  
    

}
