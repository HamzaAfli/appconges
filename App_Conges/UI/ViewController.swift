//
//  ViewController.swift
//  App_Conges
//
//  Created by Hamza on 8/23/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import EZLoadingActivity

class ViewController: UIViewController {

   // var myLoader:WavesLoader?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func startLoading(message:String)  {
        
        EZLoadingActivity.Settings.ActivityColor = UIColor(colorLiteralRed:42/255,green:25/255,blue: 53/255,alpha:1)
        EZLoadingActivity.Settings.DarkensBackground = true
        
        EZLoadingActivity.show(message, disableUI: true)
        
//        var starPath = UIBezierPath()
//        starPath.move(to: CGPoint(x:180, y:25))
//        starPath.addLine(to: CGPoint(x:195.16, y:43.53))
//        starPath.addLine(to: CGPoint(x:220.9, y:49.88))
//        starPath.addLine(to: CGPoint(x:204.54, y:67.67))
//        starPath.addLine(to: CGPoint(x:205.27, y:90.12))
//        starPath.addLine(to: CGPoint(x:180, y:82.6))
//        starPath.addLine(to: CGPoint(x:154.73, y:90.12))
//        starPath.addLine(to: CGPoint(x:155.46, y:67.67))
//        starPath.addLine(to: CGPoint(x:139.1, y:49.88))
//        starPath.addLine(to: CGPoint(x:164.84, y:43.53))
//        starPath.close()
//        UIColor.gray.setFill()
//        starPath.fill()
//        
//        let myPath = starPath.cgPath
//         myLoader = WavesLoader.showLoader(with: myPath)
//           // WavesLoader.showProgressBasedLoaderWithPath(myPath)
//
    }
    func stopLoading(succes:Bool)  {
        EZLoadingActivity.hide(succes, animated: true)
       // myLoader?.removeLoader()
    }
}
