//
//  GlobalPageViewController.swift
//  Radsomething
//
//  Created by MedAmine on 2/6/17.
//  Copyright © 2017 AppGeek+. All rights reserved.
//

import UIKit

class GlobalPageViewController: UIPageViewController,UIPageViewControllerDelegate {

    
    var tabViews:[UIViewController]=[]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        API.globalPageViewControllerReference = self
        
        //dataSource = self
        delegate = self
        
        let globalStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        tabViews.append(globalStoryBoard.instantiateViewController(withIdentifier: "NouvelleDemandeCongesVC"))
        tabViews.append(globalStoryBoard.instantiateViewController(withIdentifier: "NouvelleDemandeDeplacementVC"))
        tabViews.append(globalStoryBoard.instantiateViewController(withIdentifier: "NouvelleDemandeAutorisationVC"))
       
        
        setViewControllers([tabViews[0]], direction: .forward, animated: true, completion: nil)
        
        
    }
    
    

    
    
    func goTo(direction:PageDirections){
        
        var currentPageIndex = Int(tabViews.index(of: self.viewControllers!.first!)!)
        
        switch direction {
        case .Next:
            currentPageIndex += 1
            self.setViewControllers([tabViews[(currentPageIndex % tabViews.count)]], direction: .forward, animated: true, completion: nil)
        case .Previous:
            currentPageIndex -= 1
            self.setViewControllers([tabViews[((currentPageIndex + tabViews.count) % tabViews.count)]], direction: .reverse, animated: true, completion: nil)
        }
        
    }
    
    enum PageDirections {
        case Next
        case Previous
    }
    
    
    
    
    
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
//        let indexOfTheViewInTheTable = tabViews.index(of: viewController)!
//        
//        guard indexOfTheViewInTheTable > 0 else {
//            API.nouvelleDemandeVCReference?.DemandeTypeTF.text = API.nouvelleDemandeVCReference?.pickOption[tabViews.count - 1]
//
//            return tabViews[tabViews.count-1]
//                    }
//            API.nouvelleDemandeVCReference?.DemandeTypeTF.text = API.nouvelleDemandeVCReference?.pickOption[tabViews.count - 1]
//        return tabViews[indexOfTheViewInTheTable-1]
//    }
//    
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//        let indexOfTheViewInTheTable = tabViews.index(of: viewController)!
//        
//        guard indexOfTheViewInTheTable < tabViews.count - 1 else {
//            return tabViews[0]
//        }
//        
//        return tabViews[indexOfTheViewInTheTable+1]
//    }
    
    
}
