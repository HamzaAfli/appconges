//
//  AppDelegate.swift
//  Congés PROXYM group
//
//  Created by Hamza on 7/17/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import PSPDFKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let dev = UIDevice()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        if dev.screenType == .unknown {
            SlideMenuOptions.leftViewWidth = 540.0
        }
      
       
         return true
    }

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
      //  PSPDFKit.setLicenseKey("R+BZn67MLmAnyI8cjILxoZM9b/g5s+F4dKa9H8rUPHDlfER2TRs/ANdbti0UdsyfNTLc6rYbIIU7nEWvBgJZzBEEJagzLPcnPzfXaBx4b0FKKhNvFMDAGiVy8ELYQLkhST9heKM9Z5wvX+/sTNHcmHB86kmUcbj1KSe+shHkypsjviU7HCMmaqsFp3CrGQpQt5g+JloDh0Ukb5BtHLPDkkvr9o/w5rE/nkbIGd+reUEhI/rw46353EMtQkHxTYPvOzT9rArsmQQXAsAxtKp4l0SefJoWlAES8Z9nH+obsm0jCSKt4BnsuHrAB+dZ2KydUlULqWN+5C/th8E+1f6Dz6YpMWzYiaHQyctY5DA4/EtPJ1hiSZvyP5BgQKvcXyBT50iVyLULoqpi3mCnMuz6ZSZ6yIz62k0MFoQUul/ehtpXUp+pnFPhqHYcIa6vaPaY")
        
        
        
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}



