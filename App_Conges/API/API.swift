//
//  API.swift
//  App_Conges
//
//  Created by Hamza on 7/27/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
class API {
    static var jsessionId:String = "5BA3F133A7A18777B8A5F6C16F9D980E"
    static let BASE_URL = "preprod-tracktime.proxym-it.tn"
    static let SERVICE = "service"
    static var loginViewControllerReference:LoginViewController?
    static var nouvelleDemandeVCReference:NouvelleDemandeVC?
    static var joursFeriesViewControllerReference:JoursFeriesViewController?
    static var miseAJourCongesVC:MiseAJourCongesVC?
    static var PointageViewControllerReference:PointageViewController?
    static var adminSlideVCReference:AdminSlideVC?
    static var SlideVCReference:SlideVC?
    static var globalPageViewControllerReference:GlobalPageViewController?
    static var nouvelleDemandeCongesVCReference : NouvelleDemandeCongesVC?

    static var categories = Array<Categorie>()
    static var data = Json4Swift_Base(dictionary: [:])
    static var transactionData =  Json4Swift_Transaction(dictionary: [:])
    static var years = Array<Int>()
    static  var transaction : Transactions?
    public class func sendNouvelleDemande(onComplete:((Bool,[String:AnyObject]?)->())!){
        var categorieId:Int!
        for cat in categories {
            if cat.name == nouvelleDemandeCongesVCReference?.categorieTF.text! {
            categorieId = cat.id
                
            }
        }
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let eventDate = dateFormatter.string(from: Date())
        //true URl :   //"https://\(API.BASE_URL)/\(API.SERVICE)/transaction/createTransaction"
        // error: https://api.myjson.com/bins/16187d
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
            "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/transaction/createTransaction" , method: HTTPMethod.post, parameters: ["beginDate":nouvelleDemandeCongesVCReference!.dateDebTF.text! ,"endDate":nouvelleDemandeCongesVCReference!.dateFinTF.text! ,"eventDate":eventDate,"beginOnAfternoon":nouvelleDemandeCongesVCReference!.runkeeperSwitch1.selectedIndex ,"endOnMorning":nouvelleDemandeCongesVCReference!.runkeeperSwitch2.selectedIndex ,"motif":nouvelleDemandeCongesVCReference!.motifTF.text! ,"category":categorieId!,"j_username":loginViewControllerReference!.usernameTf.text!,"j_password":loginViewControllerReference!.passwordTf.text!,"ajax":"true"],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
            guard response.result.isSuccess else {
                onComplete(false,nil)
                
                return
            }
            let json  = response.result.value as! [String:AnyObject]
          
            onComplete(true,json)
                       
        }
    
    }
    
    public class func updateConges(){
        var categorieId:Int!
        for cat in categories {
            if cat.name == miseAJourCongesVC?.categorieTF.text! {
                categorieId = cat.id
                
            }
        }
        //true URl :   //"https://\(API.BASE_URL)/\(API.SERVICE)/transaction/createTransaction"
        // error: https://api.myjson.com/bins/16187d
        Alamofire.request("https://api.myjson.com/bins/tgzc9", method: HTTPMethod.get, parameters: ["beginDate":miseAJourCongesVC?.dateDebTF.text! as Any,"endDate":miseAJourCongesVC?.dateFinTF.text! as Any,"eventDate":"","beginOnAfternoon":miseAJourCongesVC?.runkeeperSwitch1.selectedIndex as Any,"endOnMorning":miseAJourCongesVC?.runkeeperSwitch2.selectedIndex as Any,"motif":miseAJourCongesVC?.motifTF.text! as Any,"category":categorieId],  headers: ["JSESSIONID":API.jsessionId,"Content-Type":"application/x-www-form-urlencoded"]).responseJSON { response in
            guard response.result.isSuccess else {
                SCLAlertView().showWarning("", subTitle: "Network Connection Failed ")
                return
            }
            let json  = response.result.value as! [String:AnyObject]
            if json["success"] as! Bool {
                SCLAlertView().showSuccess("", subTitle:  "La demande a été enregistrée avec succès.")
                miseAJourCongesVC?.dateFinTF.text = ""
                miseAJourCongesVC?.dateDebTF.text = ""
                miseAJourCongesVC?.categorieTF.text = ""
                miseAJourCongesVC?.runkeeperSwitch1.setSelectedIndex(0, animated: true)
                miseAJourCongesVC?.runkeeperSwitch2.setSelectedIndex(1, animated: true)
                miseAJourCongesVC?.motifTF.text = ""
            }else{
                SCLAlertView().showTitle(
                    "",subTitle: "Le solde de congés restant n'est pas suffisant pour passer cette demande.",duration: 4.0,completeText: "OK",style: SCLAlertViewStyle.error,colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
                
                
            }
            
        }
        
    }

    
    
    public class func  getHolydays(){
        joursFeriesViewControllerReference?.startLoading(message: "Telechargement...")
        
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
                       "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
    //true URl :   //
    Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/getHolydays", method: HTTPMethod.post, parameters: [:],  headers: headers).responseJSON { response in
    guard response.result.isSuccess else {
         joursFeriesViewControllerReference?.stopLoading(succes: false)
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (t:Timer) in
            SCLAlertView().showWarning("", subTitle: "Network Connection Failed !!")
        })
        return
    }
    let json  = response.result.value as! [String:AnyObject]
        sleep(5)
    if json["success"] as! Bool {
    joursFeriesViewControllerReference?.joursFeriesTab = (JsonHolydays(dictionary: json as NSDictionary)?.holydays)!
    joursFeriesViewControllerReference?.tabView.reloadData()
         joursFeriesViewControllerReference?.stopLoading(succes: true)
    }
    
    }
}
    
    
    public class func loadAllTransaction(onComplete:((Bool,[String:AnyObject]?)->())!){
       
        
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
                       "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
               
        Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/transaction/listTransactions" , method: HTTPMethod.post, parameters: ["j_username":loginViewControllerReference!.usernameTf.text!,"j_password":loginViewControllerReference!.passwordTf.text!,"ajax":"true"],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
            guard response.result.isSuccess else {
                onComplete(false,nil)
                
                return
            }
            let json  = response.result.value as! [String:AnyObject]
            
            onComplete(true,json)
            
        }
        
    }

public class func loadCategorie(onComplete:((Bool,[String:AnyObject]?)->())!){
    
    
    let user = "admin"
    let password = "Pr0xym-1T$$!!"
    let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
    let base64Credentials = credentialData.base64EncodedString(options: [])
    let headers = ["JSESSIONID":API.jsessionId,
                   "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
    
    Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/transaction/getCategories" , method: HTTPMethod.post, parameters: ["j_username":loginViewControllerReference!.usernameTf.text!,"j_password":loginViewControllerReference!.passwordTf.text!,"ajax":"true"],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
        guard response.result.isSuccess else {
            onComplete(false,nil)
            
            return
        }
        let json  = response.result.value as! [String:AnyObject]
        
        onComplete(true,json)
        
    }
    
}
    
    public class func cancelTransaction(id:Int,onComplete:((Bool,[String:AnyObject]?)->())!){
        
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
                       "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/transaction/cancelTransaction" , method: HTTPMethod.post, parameters: ["j_username":loginViewControllerReference!.usernameTf.text!,"j_password":loginViewControllerReference!.passwordTf.text!,"ajax":"true","id":id],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
            guard response.result.isSuccess else {
                onComplete(false,nil)
                
                
                return
            }
            let json  = response.result.value as! [String:AnyObject]
            
            onComplete(true,json)
            
        }
        
    }
    public class func acceptTransaction(id:Int,onComplete:((Bool,[String:AnyObject]?)->())!){
        
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
                       "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/transaction/accept" , method: HTTPMethod.post, parameters: ["j_username":loginViewControllerReference!.usernameTf.text!,"j_password":loginViewControllerReference!.passwordTf.text!,"ajax":"true","id":id],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
            guard response.result.isSuccess else {
                onComplete(false,nil)
                                
                return
            }
            let json  = response.result.value as! [String:AnyObject]
            
            onComplete(true,json)
            
        }
        
    }

    public class func getYears(onComplete:((Bool,[String:AnyObject]?)->())!){
        
        
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
                       "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/transaction/getYears" , method: HTTPMethod.post, parameters: [:],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
            guard response.result.isSuccess else {
                onComplete(false,nil)
                
                return
            }
            let json  = response.result.value as! [String:AnyObject]
            
            onComplete(true,json)
            
        }
        
    }
    
    
    public class func logout(onComplete:((Bool,[String:AnyObject]?)->())!){
        
        
        let user = "admin"
        let password = "Pr0xym-1T$$!!"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["JSESSIONID":API.jsessionId,
                       "Content-Type":"application/x-www-form-urlencoded","Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("http://\(API.BASE_URL)/\(API.SERVICE)/logout" , method: HTTPMethod.post, parameters: [:],  headers: headers).authenticate(user: "admin", password: "Pr0xym-1T$$!!").responseJSON { response in
            guard response.result.isSuccess else {
                onComplete(false,nil)
                
                return
            }
            let json  = response.result.value as! [String:AnyObject]
            
            onComplete(true,json)
            
        }
        
    }

}
