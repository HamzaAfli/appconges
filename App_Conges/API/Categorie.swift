

import Foundation
 

public class Categorie {
	public var id : Int?
	public var code : String?
	public var name : String?
	public var sign : String?
	public var nbrDaysAuthorised : Int?
	public var event : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let Categorie_list = Categorie.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Categorie Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Categorie]
    {
        var models:[Categorie] = []
        for item in array
        {
            models.append(Categorie(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let Categorie = Categorie(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Categorie Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		code = dictionary["code"] as? String
		name = dictionary["name"] as? String
		sign = dictionary["sign"] as? String
		nbrDaysAuthorised = dictionary["nbrDaysAuthorised"] as? Int
		event = dictionary["event"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.code, forKey: "code")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.sign, forKey: "sign")
		dictionary.setValue(self.nbrDaysAuthorised, forKey: "nbrDaysAuthorised")
		dictionary.setValue(self.event, forKey: "event")

		return dictionary
	}

}
