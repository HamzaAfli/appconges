

import Foundation
 


public class JsonCategorie {
	public var success : Bool?
	public var categorie : Array<Categorie>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let JsonCategorie_list = JsonCategorie.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of JsonCategorie Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [JsonCategorie]
    {
        var models:[JsonCategorie] = []
        for item in array
        {
            models.append(JsonCategorie(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let JsonCategorie = JsonCategorie(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: JsonCategorie Instance.
*/
	required public init?(dictionary: NSDictionary) {

		success = dictionary["success"] as? Bool
		if (dictionary["body"] != nil) { categorie = Categorie.modelsFromDictionaryArray(array: dictionary["body"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.success, forKey: "success")

		return dictionary
	}

}
