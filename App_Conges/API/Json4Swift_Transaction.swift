

import Foundation
 

public class Json4Swift_Transaction {
	public var success : Bool?
	public var body : Body?


    public class func modelsFromDictionaryArray(array:NSArray) -> [Json4Swift_Transaction]
    {
        var models:[Json4Swift_Transaction] = []
        for item in array
        {
            models.append(Json4Swift_Transaction(dictionary: item as! NSDictionary)!)
        }
        return models
    }


	required public init?(dictionary: NSDictionary) {

		success = dictionary["success"] as? Bool
		if (dictionary["body"] != nil) { body = Body(dictionary: dictionary["body"] as! NSDictionary) }
	}

		
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")

		return dictionary
	}

}
