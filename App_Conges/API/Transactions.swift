/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Transactions {
	public var id : Int?
	public var requestDate : String?
	public var beginDate : String?
	public var endDate : String?
	public var eventDate : String?
	public var beginOnAfternoon : Bool?
	public var endOnMorning : Bool?
	public var nbrDaysFirstYear : Int?
	public var nbrDaysSecondYear : Int?
	public var totalDays : Int?
	public var status : String?
	public var employeeId : Int?
	public var employeeUsername : String?
	public var categoryId : Int?
	public var categoryName : String?
	public var isEvent : Bool?
	public var event : String?
	public var creatorId : Int?
	public var creatorUsername : String?
	public var updatedAt : String?
	public var updatedBy : String?
	public var updatedById : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let transactions_list = Transactions.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Transactions Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Transactions]
    {
        var models:[Transactions] = []
        for item in array
        {
            models.append(Transactions(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let transactions = Transactions(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Transactions Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		requestDate = dictionary["requestDate"] as? String
		beginDate = dictionary["beginDate"] as? String
		endDate = dictionary["endDate"] as? String
		eventDate = dictionary["eventDate"] as? String
		beginOnAfternoon = dictionary["beginOnAfternoon"] as? Bool
		endOnMorning = dictionary["endOnMorning"] as? Bool
		nbrDaysFirstYear = dictionary["nbrDaysFirstYear"] as? Int
		nbrDaysSecondYear = dictionary["nbrDaysSecondYear"] as? Int
		totalDays = dictionary["totalDays"] as? Int
		status = dictionary["status"] as? String
		employeeId = dictionary["employeeId"] as? Int
		employeeUsername = dictionary["employeeUsername"] as? String
		categoryId = dictionary["categoryId"] as? Int
		categoryName = dictionary["categoryName"] as? String
		isEvent = dictionary["isEvent"] as? Bool
		event = dictionary["event"] as? String
		creatorId = dictionary["creatorId"] as? Int
		creatorUsername = dictionary["creatorUsername"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		updatedBy = dictionary["updatedBy"] as? String
		updatedById = dictionary["updatedById"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.requestDate, forKey: "requestDate")
		dictionary.setValue(self.beginDate, forKey: "beginDate")
		dictionary.setValue(self.endDate, forKey: "endDate")
		dictionary.setValue(self.eventDate, forKey: "eventDate")
		dictionary.setValue(self.beginOnAfternoon, forKey: "beginOnAfternoon")
		dictionary.setValue(self.endOnMorning, forKey: "endOnMorning")
		dictionary.setValue(self.nbrDaysFirstYear, forKey: "nbrDaysFirstYear")
		dictionary.setValue(self.nbrDaysSecondYear, forKey: "nbrDaysSecondYear")
		dictionary.setValue(self.totalDays, forKey: "totalDays")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.employeeId, forKey: "employeeId")
		dictionary.setValue(self.employeeUsername, forKey: "employeeUsername")
		dictionary.setValue(self.categoryId, forKey: "categoryId")
		dictionary.setValue(self.categoryName, forKey: "categoryName")
		dictionary.setValue(self.isEvent, forKey: "isEvent")
		dictionary.setValue(self.event, forKey: "event")
		dictionary.setValue(self.creatorId, forKey: "creatorId")
		dictionary.setValue(self.creatorUsername, forKey: "creatorUsername")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.updatedBy, forKey: "updatedBy")
		dictionary.setValue(self.updatedById, forKey: "updatedById")

		return dictionary
	}
    init() {
        
    }

}
