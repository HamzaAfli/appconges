/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Employee {
	public var id : Int?
	public var employee : String?
	public var hmTravail : Int?
	public var joursTravaille : Int?
	public var heureTravaille : Int?
	public var noteMoyenne : Int?
	public var retard : Int?
	public var reportDays : Int?
	public var yearDays : Int?
	public var recupDays : Int?
	public var takenDays : Int?
	public var eventDays : Int?
	public var diseaseDays : Int?
	public var unpaidDays : Int?
	public var solde : Int?
	public var matricule : Int?
	public var email : String?
	public var displayName : String?
	public var enabled : String?
	public var department : Department?
	public var isAdmin : Bool?
	public var admin : Admin?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let employee_list = Employee.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Employee Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Employee]
    {
        var models:[Employee] = []
        for item in array
        {
            models.append(Employee(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let employee = Employee(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Employee Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		employee = dictionary["employee"] as? String
		hmTravail = dictionary["HmTravail"] as? Int
		joursTravaille = dictionary["JoursTravaille"] as? Int
		heureTravaille = dictionary["HeureTravaille"] as? Int
		noteMoyenne = dictionary["NoteMoyenne"] as? Int
		retard = dictionary["Retard"] as? Int
		reportDays = dictionary["reportDays"] as? Int
		yearDays = dictionary["yearDays"] as? Int
		recupDays = dictionary["recupDays"] as? Int
		takenDays = dictionary["takenDays"] as? Int
		eventDays = dictionary["eventDays"] as? Int
		diseaseDays = dictionary["diseaseDays"] as? Int
		unpaidDays = dictionary["unpaidDays"] as? Int
		solde = dictionary["solde"] as? Int
		matricule = dictionary["matricule"] as? Int
		email = dictionary["email"] as? String
		displayName = dictionary["displayName"] as? String
		enabled = dictionary["enabled"] as? String
		if (dictionary["department"] != nil) { department = Department(dictionary: dictionary["department"] as! NSDictionary) }
		isAdmin = dictionary["isAdmin"] as? Bool
		if (dictionary["admin"] != nil) { admin = Admin(dictionary: dictionary["admin"] as! NSDictionary) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.employee, forKey: "employee")
		dictionary.setValue(self.hmTravail, forKey: "HmTravail")
		dictionary.setValue(self.joursTravaille, forKey: "JoursTravaille")
		dictionary.setValue(self.heureTravaille, forKey: "HeureTravaille")
		dictionary.setValue(self.noteMoyenne, forKey: "NoteMoyenne")
		dictionary.setValue(self.retard, forKey: "Retard")
		dictionary.setValue(self.reportDays, forKey: "reportDays")
		dictionary.setValue(self.yearDays, forKey: "yearDays")
		dictionary.setValue(self.recupDays, forKey: "recupDays")
		dictionary.setValue(self.takenDays, forKey: "takenDays")
		dictionary.setValue(self.eventDays, forKey: "eventDays")
		dictionary.setValue(self.diseaseDays, forKey: "diseaseDays")
		dictionary.setValue(self.unpaidDays, forKey: "unpaidDays")
		dictionary.setValue(self.solde, forKey: "solde")
		dictionary.setValue(self.matricule, forKey: "matricule")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.displayName, forKey: "displayName")
		dictionary.setValue(self.enabled, forKey: "enabled")
		dictionary.setValue(self.department?.dictionaryRepresentation(), forKey: "department")
		dictionary.setValue(self.isAdmin, forKey: "isAdmin")
		dictionary.setValue(self.admin?.dictionaryRepresentation(), forKey: "admin")

		return dictionary
	}

}
